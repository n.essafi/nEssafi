QCM 1 - Qu'est-ce que l'algorithmique ?

f)Une suite d'opérations binaires et conditionnelles.     


QCM 2 - Qu'est-ce qu'une Variable ?

d)Une valeur dans mon programme;




QCM 3 - Qu'est-ce qu'un Type ?

c)Une précision sur le contenu d'une variable;




QCM 4 - Qu'est-ce qu'une Condition ?

b)Un test d'expression booléenne;



QCM 5 - Qu'est-ce qu'une Boucle ?

c)Une itération sur une suite d'éléments;

f)Une suite d'instruction qui n'a lieu que dans certains cas.



QCM 6 - Qu'est-ce que la Complexité ?

b)La totalité des instructions élémentaires d'un algorithme;

c)Le nombre de boucle d'un programme;




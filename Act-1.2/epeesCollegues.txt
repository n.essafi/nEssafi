Algoritme epeesCollegues
Type Tab :tableau [1..N] de chaine

Variables:
x :boolean
i,j,N:entier
T:Tab

Debut

pour i allant de N à 2 par pas de -1 faire
     pour j allant de 1 à i-1 faire

           x <---- comparaison (T[j],T[j+1])
         
         si(x = True) faire
            temp <-- T[j]
            T[j] <--T[j+1]
            T[j+1] <-- temp
         finSi
    finPour
finPour

Fin


Fonction comparaison (ch1,ch2 :chaine) :boolean
Variables
k,x, y :entier
permutation : boolean

Debut

 k <-- 0

 x <-- codeasci(ch1[0])

 y <-- codeasci(ch2[0])

permutation <-- false

  Tantque (permutation = false && x >= y && k<lang(ch1) && k<leng(ch2) )

                si ( x > y )

                permutation  = true             

                sinon si  ( x == y )

                   k ++

                   x <-- codeasci(ch1[k])

                   y <-- codeasci(ch2[k])

                finsi

   finTnatque

 retourner (permutation)

Fin

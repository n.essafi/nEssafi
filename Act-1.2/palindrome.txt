fonction Palindrome ()
{cet algoritme affiche si le mot fourni par l'utilisateur est un palindrome ou non}

Variables
CH : chaine
Egaux : boolean
i, count : entier

Debut 

Ecrire "Entrer un mot : "
Lire CH

Egaux <-- True
count <-- 0
i <--1

Tantque (i <= lang(CH) Div 2 && Egaux=True faire)
        Egaux <--false
        
        si CH[i] = CH[lang(CH)-i]
           count++
           Egaux <-- True
          i++
        finSi
     
        
finTantque

  si  (Egaux = True && count = lang(CH) Div 2)
      Ecrire " votre mot " , CH, " est palindrome"
  sinon
    Ecrire " votre mot " , CH, " n'est pas un palindrome"
  finSi

fintantque

 Fin



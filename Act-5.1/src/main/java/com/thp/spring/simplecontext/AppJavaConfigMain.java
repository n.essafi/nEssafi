package com.thp.spring.simplecontext;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class AppJavaConfigMain {

	public static void main(String[] args) {

		// load the spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppMoussaillonConfig.class);

		// retrieve Bean from spring container
		Moussaillon moussaillon = context.getBean("moussaillon", Moussaillon.class);

		// call methods on the bean
		System.out
				.println("Le prénom du moussaillon : " + moussaillon.getFirstName() + "\n" + "Le nom du moussaillon : "
						+ moussaillon.getLastName() + "\n" + "Configuration utilisée : " + moussaillon.getConfig()
						+ "\n" + "Bravo, vous venez de créer votre premier contexte Spring en Java !!");
		// close the context
		context.close();

	}

}

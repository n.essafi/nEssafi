package com.thp.project.vintud.entity.impl;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import com.thp.project.vintud.dao.factory.FactoryDao;
import com.thp.project.vintud.dao.impl.UserDaoImpl;
import com.thp.project.vintud.entity.User;

public class SignInForm {

	private static final String CHAMP_EMAIL = "email";
	private static final String CHAMP_PASSWORD = "password";
	private String resultat;
	private Map<String, String> erreurs = new HashMap<String, String>();
	final FactoryDao factoryDao = new FactoryDao();

	public Map<String, String> getErreurs() {
		return erreurs;
	}

	public String getResultat() {
		return resultat;
	}

	public User validateClient(HttpServletRequest request) {

		User client = new User();
		String email = getValeurChamp(request, CHAMP_EMAIL);
		String password = getValeurChamp(request, CHAMP_PASSWORD);
		try {
			validationEmailAndPassword(email,password);
		} catch (Exception e) {
			setErreur(CHAMP_EMAIL, e.getMessage());
		}
		
		client.setMail(email);

		if (erreurs.isEmpty()) {
			resultat = "Succ�s de la cr�ation du client.";
		} else {
			resultat = "Veuillez verifier votre login et mot de passe";
		}

		return client;

	}

	private void validationEmailAndPassword(String email, String password) throws Exception {
		UserDaoImpl user = factoryDao.getUserDao();
		if (email != null && !email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
			throw new Exception("Merci de saisir une adresse mail valide.");
		} else if (!user.login(email, password)) {
			throw new Exception("not match");
		}

	}
	
	
	

	/*
	 * Ajoute un message correspondant au champ sp�cifi� � la map des erreurs.
	 */
	private void setErreur(String champ, String message) {
		erreurs.put(champ, message);
	}

	/*
	 * M�thode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	private static String getValeurChamp(HttpServletRequest request, String nomChamp) {
		String valeur = request.getParameter(nomChamp);
		if (valeur == null || valeur.trim().length() == 0) {
			return null;
		} else {
			return valeur;
		}
	}

}

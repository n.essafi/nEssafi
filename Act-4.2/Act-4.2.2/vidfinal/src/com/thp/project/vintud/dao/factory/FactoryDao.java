package com.thp.project.vintud.dao.factory;

import java.sql.Connection;

import com.thp.project.vintud.db.ConnectionManager;
import com.thp.project.vintud.dao.IAdDao;
import com.thp.project.vintud.dao.IFavoriteDao;
import com.thp.project.vintud.dao.ISearchDao;
import com.thp.project.vintud.dao.IUserDao;
import com.thp.project.vintud.dao.impl.AdDaoImpl;
import com.thp.project.vintud.dao.impl.FavoriteDaoImpl;
import com.thp.project.vintud.dao.impl.SearchDaoImpl;
import com.thp.project.vintud.dao.impl.UserDaoImpl;

public class FactoryDao {

	// retourne un objet interagissant avec la base de donn�e
	public FavoriteDaoImpl getFavoriteDao() {
		return new FavoriteDaoImpl(this);
	}

	// retourne un objet interagissant avec la base de donn�e
	public SearchDaoImpl getSearchDao() {
		return new SearchDaoImpl(this);
	}
	// retourne un objet interagissant avec la base de donn�e
	public AdDaoImpl getAdDao() {
		return new AdDaoImpl(this);
	}
	// retourne un objet interagissant avec la base de donn�e
	public UserDaoImpl getUserDao() {
		return new UserDaoImpl(this);
	}

	public static Connection getConnection() {
		return ConnectionManager.getInstance();
	}

}
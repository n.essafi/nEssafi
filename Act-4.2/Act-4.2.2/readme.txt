Le projet vintud-web est un projet web dynamique qui presente un site d'Ecommerce dans lequel vous pouvez creer et lister des annonces:
il contient :
le package com.thp.project.vintud.entity : qui contient les javaBeans
le package com.thp.project.vintud.entity.impl :qui contient les exceptions des forms login et register
le package com.thp.project.vintud.dao.controller: qui contient les servlets
le package com.thp.project.vintud.dao : qui contients les interfaces
le package com.thp.project.vintud.dao.impl :qui contient l'implementation des interfaces.
le package com.thp.project.vintud.db :contient le singleton;
le package com.thp.project.vintud.dao.factory : contient factory de DAO;
le dossier lib : contient les jar necessaires
les fichier jsp et html se trouvent dans le dossier WEB-INF.

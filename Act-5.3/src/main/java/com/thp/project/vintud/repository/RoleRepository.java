package com.thp.project.vintud.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Role findByIdRole(int id);

}

package com.thp.project.vintud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Search;

@Repository

public interface SearchRepository extends JpaRepository<Search, Long> {
	

}

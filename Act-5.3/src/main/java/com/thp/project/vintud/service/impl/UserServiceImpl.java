package com.thp.project.vintud.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.UserRepository;
import com.thp.project.vintud.service.UserService;

@Service

public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	public void createUser(User user) {

		userRepository.save(user);
		System.out.println("ceation user ok .");
	}

	public void updateUser(User user, String mail, String pseudo, String password) {

		user.setPseudo(pseudo);
		user.setMail(mail);
		user.setPassword(password);

		userRepository.save(user);

		System.out.println("Update ok.");
	}

	public boolean login(String mail, String password) {

		boolean res = false;
		List<User> result = userRepository.findByMailAndPassword(mail, password);

		if (!result.isEmpty()) {
			res = true;
		}

		if (res) {
			System.out.println("Login suecces");
		} else {
			System.out.println("Login failed");
		}
		return res;
	}

	@Override
	public void updateRole(User user, Role role) {
		user.setRole(role);
		userRepository.save(user);

	}

	@Override
	public void delete(User user) {
		userRepository.delete(user);

	}

	@Override
	public User getbyId(int id) {

		return userRepository.findOne(id);
	}

	@Override
	public List<User> display() {

		return userRepository.findAll();
	}

}

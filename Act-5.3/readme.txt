Le projet vintud-spring-JPA est un maven-projet  qui presente un site d'Ecommerce dans lequel vous pouvez manipuler les annonces:
il contient :
le package com.thp.project.vintud.entity : qui contient les javaBeans
le package com.thp.project.vintud.repository : qui contients les repositries
le package com.thp.project.vintud.services :qui contient l'implementation des interfaces.
le package com.thp.project.vintud.services.impl :qui contient l'implementation des interfaces.
pom.xml: qui contient les dependeces
le dossier Meta-inf dans ressources: contient le fichier persistence.xml

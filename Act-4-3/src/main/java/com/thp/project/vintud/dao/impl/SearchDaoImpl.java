package com.thp.project.vintud.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.thp.project.vintud.dao.ISearchDao;
import com.thp.project.vintud.dao.factory.FactoryDao;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.Search;

public class SearchDaoImpl implements ISearchDao {

	private FactoryDao factoryDao;

	public SearchDaoImpl(FactoryDao factoryDao) {
		this.factoryDao = factoryDao;
	}

	public int create(Search search) throws SQLException {
		Connection connect = FactoryDao.getConnection();
		Statement stm = null;
		ResultSet rs = null;
		int lastKey = 1;

		stm = connect.createStatement();

		String query = "INSERT INTO  search(type , size , color, min_price,max_price) " + "VALUES ('" + search.getType()
				+ "','" + search.getSize() + "','" + search.getColor() + "','" + search.getMinPrice() + "','"
				+ search.getMaxPrice() + "');";
		stm.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

		ResultSet keys = stm.getGeneratedKeys();

		while (keys.next()) {
			lastKey = keys.getInt(1);
		}

		search.setIdSearch(lastKey);

		return lastKey;

	}

	public int delete(int id) throws SQLException {
		Connection connect = FactoryDao.getConnection();
		PreparedStatement ps = null;

		String query = "DELETE FROM search WHERE id =" + id + ";";
		return connect.createStatement().executeUpdate(query);

	}

// méthode de lancement d'une recherche préalablement créée
	public String launch(int id) throws SQLException {
		Connection connect = FactoryDao.getConnection();
		Statement stm = null;
		String results = "";
		String query ="SELECT a.title, a.description,a.price, CONCAT(u.firstname,' ',u.name) AS 'Buyer Name' FROM announcement a\n" + 
				" RIGHT JOIN user u ON a.user_id=u.id\n" + 
				"RIGHT JOIN search s ON (a.price BETWEEN s.min_price AND s.max_price) AND (LOWER(a.description) LIKE LOWER(CONCAT('%',s.size,'%'))) AND (LOWER(a.title) LIKE LOWER(CONCAT('%',s.type,'%')) )\n" + 
				" WHERE s.id="+id+";";

		stm = connect.createStatement();
		ResultSet rs = stm.executeQuery(query);

		while (rs.next()) {
			results = "Title of the ad:\n" + rs.getString(1) + ".\nDescription:\n" + rs.getString(2) + ".\nPrice: "
					+ rs.getInt(3) + ".\nBuyer Name: " + rs.getString(4) + ".";

		}
		return results;

	}

	public void update(Search obj) throws SQLException {

	}
}

package com.thp.project.vintud.entity;

public class Category {
	private String nameCategory;
	private String descriptionCategory;
	private int idCategory;

	public Category() {
	}

	public Category(int id,String nameCategory, String descriptionCategory) {
		this.nameCategory = nameCategory;
		this.descriptionCategory = descriptionCategory;
		this.idCategory=id;
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public String getDescriptionCategory() {
		return descriptionCategory;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public void setDescriptionCategory(String descriptionCategory) {
		this.descriptionCategory = descriptionCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

}

package com.thp.spring.simplecontext;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.thp.spring.simplecontext.entity.Moussaillon;

public class AppXMLMain {

	public static void main(String[] args) {

		// load the spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// retrieve Bean from spring container
		Moussaillon moussaillon = context.getBean("moussaillon", Moussaillon.class);

		// call methods on the bean
		System.out
				.println("Le prénom du moussaillon : " + moussaillon.getFirstName() + "\n" + "Le nom du moussaillon : "
						+ moussaillon.getLastName() + "\n" + "Configuration utilisée : " + moussaillon.getConfig()
						+ "\n" + " Bravo, vous venez de créer votre premier contexte Spring en XML!!");
		// close the context
		context.close();

	}

}

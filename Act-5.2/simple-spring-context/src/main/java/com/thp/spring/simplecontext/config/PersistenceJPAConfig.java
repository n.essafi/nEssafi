package com.thp.spring.simplecontext.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@Configuration
@EnableTransactionManagement

@ComponentScans(value = { @ComponentScan("com.thp.spring.simplecontext.dao"),
     @ComponentScan("com.thp.spring.simplecontext.dao.impl") })
public class PersistenceJPAConfig {

	
	@Bean
	   public LocalEntityManagerFactoryBean geEntityManagerFactoryBean() {
	      LocalEntityManagerFactoryBean factoryBean = new LocalEntityManagerFactoryBean();
	      factoryBean.setPersistenceUnitName("LOCAL_PERSISTENCE");
	      return factoryBean;
	   }

	   @Bean
	   public JpaTransactionManager geJpaTransactionManager() {
	      JpaTransactionManager transactionManager = new JpaTransactionManager();
	      transactionManager.setEntityManagerFactory(geEntityManagerFactoryBean().getObject());
	      return transactionManager;
	   }
	
	 
//	   @Bean
//	   public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//	      LocalContainerEntityManagerFactoryBean em 
//	        = new LocalContainerEntityManagerFactoryBean();
//	      em.setDataSource(dataSource());
//	      em.setPackagesToScan(new String[] { "com.thp.spring.simplecontext" });
//	 
//	      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//	      em.setJpaVendorAdapter(vendorAdapter);
//	      em.setJpaProperties(additionalProperties());
//	 
//	      return em;
//	   }
//	    
//	   @Bean
//	   public DataSource dataSource(){
//	       DriverManagerDataSource dataSource = new DriverManagerDataSource();
//	       dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
//	       dataSource.setUrl("jdbc:mysql://localhost:3306/ bateaumoussaillonJDBC");
//	       dataSource.setUsername( "root" );
//	       dataSource.setPassword( "" );
//	       return dataSource;
//	   }
//	   
//	   @Bean
//	   public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
//	       JpaTransactionManager transactionManager = new JpaTransactionManager();
//	       transactionManager.setEntityManagerFactory(emf);
//	    
//	       return transactionManager;
//	   }
//	    
//	   @Bean
//	   public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
//	       return new PersistenceExceptionTranslationPostProcessor();
//	   }
//	    
//	   Properties additionalProperties() {
//	       Properties properties = new Properties();
//	       properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
//	       properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
//	           
//	       return properties;
//	   }
	 
	}



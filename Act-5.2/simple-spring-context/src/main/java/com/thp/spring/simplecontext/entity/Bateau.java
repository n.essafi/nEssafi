package com.thp.spring.simplecontext.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
@Entity
@Table (name ="bateau")
@NamedQueries({
	 @NamedQuery(name = "Bateau.findById",
           query = "SELECT b FROM Bateau b WHERE b.id = ?1"),
	 @NamedQuery(name = "Bateau.findAll",
        query = "SELECT b FROM Bateau b ")
})
	

public class Bateau implements Serializable {
	
	

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String name;
	
	private String type;
	
	private double taille;
	
	@OneToMany(mappedBy = "bateau", fetch = FetchType.LAZY)
	private List<Moussaillon> moussaillons;
	
	
	public Bateau() {
		
	}


	public Bateau(String name, String type, double taille) {
		super();
		this.name = name;
		this.type = type;
		this.taille = taille;
	}


	public int getId() {
		return id;
	}


	public void setIdBateau(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public double getTaille() {
		return taille;
	}


	public void setTaille(double taille) {
		this.taille = taille;
	}


	public List<Moussaillon> getMoussaillons() {
		return moussaillons;
	}


	public void setMoussaillons(List<Moussaillon> moussaillons) {
		this.moussaillons = moussaillons;
	}
	
	
	
	@Override
	public String toString() {
		return "Bateau [id=" + id + ", name=" + name + ", type=" + type + ", taille=" + taille + "]";
	}

	
	
	
	
	

}

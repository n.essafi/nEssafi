package com.thp.spring.simplecontext.main;

import java.sql.SQLException;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.thp.spring.simplecontext.config.PersistenceJPAConfig;
import com.thp.spring.simplecontext.dao.BateauDao;
import com.thp.spring.simplecontext.entity.Bateau;

public class AppMain {

	public static void main(String[] args) throws SQLException {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PersistenceJPAConfig.class);

		BateauDao bateauDao = context.getBean(BateauDao.class);

		System.out.println(bateauDao.create(new Bateau("the best", "Navire Touristique", 200.0)));
		System.out.println(bateauDao.create(new Bateau("Black Pearl,", "Navire de Guerre", 150.0)));
		System.out.println(bateauDao.findById(2));
		System.out.println(bateauDao.findAll());
	}

}

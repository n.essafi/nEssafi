package com.thp.spring.simplecontext.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thp.spring.simplecontext.dao.BateauDao;
import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.entity.Moussaillon;

@Repository
@Transactional
public class BateauDaoImpl implements BateauDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public int create(Bateau bateau) {
		em.persist(bateau);
		return bateau.getId();

	}

	@Override
	public Bateau update(Bateau bateau, String name, String type, double taille) {
		return null;
	}

	@Override
	public void delete(Bateau bateau) {

	}

	@Override
	public Bateau findById(int id) {
		@SuppressWarnings("unchecked")
		List<Bateau> bateaux = em.createNamedQuery("Bateau.findById").setParameter(1, id).getResultList();
		return bateaux.get(0);
	}

	@Override
	public Bateau findByMoussaillon(Moussaillon moussaillon) {

		return null;
	}

	@Override
	public List<Bateau> findAll() {

		List<Bateau> bateaux = em.createNamedQuery("Bateau.findAll").getResultList();

		return bateaux;

	}

}

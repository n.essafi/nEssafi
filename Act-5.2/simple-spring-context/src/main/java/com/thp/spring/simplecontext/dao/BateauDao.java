package com.thp.spring.simplecontext.dao;

import java.util.List;

import com.thp.spring.simplecontext.entity.Bateau;
import com.thp.spring.simplecontext.entity.Moussaillon;

//Interface BateauDAO
public interface BateauDao {

	int create(Bateau bateau);

	Bateau update(Bateau bateau, String name, String type, double taille);

	void delete(Bateau bateau);

	Bateau findById(int id);

	Bateau findByMoussaillon(Moussaillon moussaillon);

	List<Bateau> findAll();
}
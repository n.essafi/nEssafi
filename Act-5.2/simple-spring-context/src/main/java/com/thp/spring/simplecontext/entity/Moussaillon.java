package com.thp.spring.simplecontext.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "moussaillon")
public class Moussaillon implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer idMoussaillon;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	private String config;
	
	@ManyToOne
	@JoinColumn(name = "bateau_id")
	private Bateau bateau;

	public Moussaillon() {
		super();

	}

	public Moussaillon(String firstName, String lastName, String config) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.config = config;
	}

	public Moussaillon(String firstName, String lastName, String config, Bateau bateau) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.config = config;
		this.bateau = bateau;
	}

// Getters and Setters
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public Integer getIdMoussaillon() {
		return idMoussaillon;
	}

	public void setIdMoussaillon(Integer idMoussaillon) {
		this.idMoussaillon = idMoussaillon;
	}

	public Bateau getBateau() {
		return bateau;
	}

	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}

}

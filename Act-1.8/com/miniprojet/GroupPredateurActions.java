package com.miniprojet;

public interface GroupPredateurActions {

	void addPredateur(Predateur animal);

	void deletePredateur(Predateur animal);

    public String toString();

}

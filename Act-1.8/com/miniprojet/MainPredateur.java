package com.miniprojet;

public class MainPredateur {

	public static void main(String[] args) {

		Lion l1 = new Lion("male", Age.ADULTE, 10, true, Domination.ALPHA, true, false, false);
		Lion l2 = new Lion("femelle", Age.ADULTE, 10, true, Domination.BETA, true, false, false);
		Lion l3 = new Lion("femelle", Age.ADULTE, 10, true, Domination.GAMA, true, false, false);
		l1.emettreSon(2);
		l2.emettreSon(2);

		Ours O1 = new Ours("male", Age.ADULTE, 10, 50, "moyenne", false);
		O1.emettreSon(4);

		GroupLions groupe1 = new GroupLions("Afrique","fort");
		groupe1.addPredateur(l1);
		groupe1.addPredateur(l2);
		groupe1.addPredateur(l3);
		groupe1.deletePredateur(l2);
		System.out.println(groupe1);
		
		
		groupe1.seReproduire(l1,l3);
	}

}

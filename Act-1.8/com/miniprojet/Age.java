package com.miniprojet;

public enum Age {
	
	JEUNE("il n'est pas assez capable de chasser" ,"Il court lentement", "Il est encore jeune pour se reproduire"), 
	
	ADULTE("il est capable de chasser rapidement", "Il court rapidement", "Il est capable de se reproduire"), 
	
	VIEUX("il chasse d'une maniere tres difficile", "Il court difficilement", "Il n'est pas capable de se reproduire");
	
	private String capacitChasse;
	private String capaciteDECourir;
	private String capaciteDEProduction;
	

	private Age(String capacitChasse, String capaciteDECourir,String capaciteDEProduction) {
		this.capacitChasse = capacitChasse;
		this.capaciteDECourir = capaciteDECourir;
		this.capaciteDEProduction = capaciteDEProduction ;
	}

	public String getCapacitChasse() {
		return capacitChasse;
	}

	

	public String getCapaciteDECourir() {
		return capaciteDECourir;
	}

	

	public String getCapaciteDEProduction() {
		return capaciteDEProduction;
	}

	
	

}

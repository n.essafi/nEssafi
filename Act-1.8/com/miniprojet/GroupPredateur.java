package com.miniprojet;

import java.util.Arrays;

public abstract class GroupPredateur implements GroupPredateurActions {

	private int taille ;
	private String zoneGeo;
	private Predateur[] predateurList;


	public GroupPredateur(String zoneGeo) {
		super();
		this.taille= 0;
		this.setZoneGeo(zoneGeo);
		this.predateurList = new Predateur[10];
		
	}
	
	public String getZoneGeo() {
		return zoneGeo;
	}

	public void setZoneGeo(String zoneGeo) {
		this.zoneGeo = zoneGeo;
	}
	
	

	public void addPredateur(Predateur animal) {
		this.predateurList[this.taille] = animal;
		this.taille++;

	}

	public void deletePredateur(Predateur animal) {

		for (int i = 0; i < this.taille; i++) {

			if (this.predateurList[i].equals(animal)) {

				for (int j = i; j < this.taille; j++) {

					this.predateurList[i] = this.predateurList[i + 1];
				}
				taille--;
			}
			
			
		}

	}

	public Predateur[] getPredateurList() {
		return predateurList;
	}

	public void setPredateurList(Predateur[] predateurList) {
		this.predateurList = predateurList;
	}

	public int getTaille() {
		return taille;
	}


	

}

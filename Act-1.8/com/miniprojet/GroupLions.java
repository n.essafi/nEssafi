package com.miniprojet;

import java.util.Arrays;

public class GroupLions extends GroupPredateur implements GroupLionsAction {

	private Lion[] coupleDominant = new Lion[2];
	private String puissance;

	public GroupLions(String zoneGeo, String puissance) {
		super(zoneGeo);
		this.puissance = puissance;
	}

	public String getPuissance() {
		return puissance;
	}

	public void setPuissance(String puissance) {
		this.puissance = puissance;
	}

	@Override
	public String toString() {
		return "GroupLions [taille=" + this.getTaille() + ", puissance=" + puissance + "]"
				+ Arrays.toString(this.getPredateurList());
	}

	@Override
	public void seReproduire(Lion lion1, Lion lion2) {
	    if (!lion1.getSexe().equalsIgnoreCase(lion2.getSexe()))
		{
	    	System.out.println("Les lions se reproduisent");
		}
	    else {
	    	System.out.println("Les lions ne peuvent pas se produirent");
	    }
	    
	}



}

package com.miniprojet;


public interface PredateurAction {
	void nourrir();
	void chassser();
	void courir();
	void seReproduire();
	void emettreSon(int type);

}

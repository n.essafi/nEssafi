export interface Post{
    title : String;
    content : String ;
    date : Date;
    loveIts : number;
}
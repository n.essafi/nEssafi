import { Component, OnInit , Input} from '@angular/core';

import { Post } from "../post";
import { POSTS } from "../posts";
import { PostService } from '../services/post.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  
  @Input() post ;
  constructor(private postService: PostService) { }

  ngOnInit(): void {
  }

  onDelete(post:Post){
    this.postService.removePost(post);

  }
  
  onLike(){
    this.post.loveIts ++ ;
  }

  onDislike(){
    this.post.loveIts -- ;
  }

  getColor(){
    if (this.post.loveIts>0) {
      return 'rgba(0, 255, 0, 0.2)';
    } 
    else if(this.post.loveIts<0){
      return 'rgba(255, 0, 0, 0.2)';
    }else{
      return 'rgba(0, 0, 0, 0.0)';
    }
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { EditPostComponent } from './edit-post/edit-post.component';
import { PostService } from './services/post.service';


const appRoutes :Routes=[
  {path:'', component:PostListComponent},
  {path:'new', component: EditPostComponent},
  {path:'posts', component: PostListComponent},
]


@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostListItemComponent,
    EditPostComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { POSTS } from "../posts";
import { Subject } from 'rxjs';
import { Post } from '../post';

export class PostService{
    postSubject = new Subject<any[]>();
    posts = POSTS;

    emitPostSubject() {
        this.postSubject.next(this.posts.slice());
    }
    addPost(title:string, content:string){
        const postObject = {
            title : '',
            content : '' ,
            date : new Date(),
            loveIts : 0
        };
        postObject.title=title;
        postObject.content=content;
        this.posts.push(postObject);
        console.log(this.posts.length)
        this. emitPostSubject() ;
            
    }
    removePost(post:Post){
        const postIndexToRemove = this.posts.findIndex(
            (postElem) =>{
                if(postElem === post){
                    return true;
                }
            }
        );
        this.posts.splice(postIndexToRemove,1);
        this. emitPostSubject() ;
    }


}


import java.io.File;
import java.io.FilenameFilter;


public class ApplicationFile {

	public static void listFiles(String chemin) {
		File folder = new File(chemin);
		String[] files = folder.list();

		for (String file : files) {
			System.out.println(file);
		}

	}

	public static void filterFiles(String chemin) {

		File dir = new File(chemin);
		File[] files = dir.listFiles();

		for (File file : files) {

			if (file.isDirectory()) {

				String[] files1 = file.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						boolean value;
						// return files only that begins with test
						if(name.endsWith(".java")){
							value=true;
						}
						else{
							value=false;
						}
						return value;
					}
				});
				for(String s:files1){
					System.out.println(s);
				}
			} 
			
		}

		
	}
	
	public static void listAllFiles(String chemin) {

		File dir = new File(chemin);
		File[] files = dir.listFiles();

		for (File file : files) {

			if (file.isDirectory()) {

				listFiles(file.getAbsolutePath());
			} else {

				System.out.println(file.getName());
			}
		}

	}
	

	public static void main(String[] args) {

		String CurrentDir = (".");
		System.out.println("------------la liste des fichiers du repertoire courant  -------- ");
		listFiles(CurrentDir);
		
		System.out.println("------------la liste des tous les fichiers dans le repertoire courant et sous repertoire  --------");
		listAllFiles(CurrentDir);
		
		System.out.println("------------la liste des tous les fichiers filtr�s .java --------");
		filterFiles(CurrentDir);
	}
}








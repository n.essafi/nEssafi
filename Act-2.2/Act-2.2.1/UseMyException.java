import java.util.Scanner;

public class UseMyException {

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		System.out.println("Saisir un nombre entre 10 et 30 inclus: ");
		int nb = sc.nextInt();
		if (nb < 10 || (nb > 30)) {
			throw new Exception("The value is not in the allowed interval");
		}
		System.out.println("nombre = " + nb);

	}
}

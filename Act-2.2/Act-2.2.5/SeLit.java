import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SeLit {

	static void lecture(Scanner source) {

		while (source.hasNextLine()) {
			String s = source.nextLine();
			System.out.println("\"LU:\"" + s);
		}
	}

	public static void lectureInverse(Scanner source) {

		List<String> tmp = new ArrayList<>();

		while (source.hasNextLine()) {
			String s = source.nextLine();
			tmp.add(s);
		}

		for (int i = tmp.size() - 1; i >= 0; i--) {
			System.out.println("\"LU ENVERS:\""+tmp.get(i));
		}
	}

	public static void lectureEcriture(Scanner source) {

		List<String> tmp = new ArrayList<String>();

		while (source.hasNextLine()) {
			String s = source.nextLine();
			tmp.add(s);
		}

		for (int i = 0; i < tmp.size(); i++) {
			Writer wr = null;
			try {
				String filename = "monFichier_L" + i + ".txt";
				wr = new FileWriter(filename);
			} catch (IOException e) {

				e.printStackTrace();
			}
			try {
				wr.write(tmp.get(i));
				wr.flush();
				wr.close();
			} catch (IOException e) {

				e.printStackTrace();
			}

		}
	}

	public static void main(String[] args) {
	

		File file = new File(Paths.get(".").toAbsolutePath().toString()+"\\src\\SeLit.java");

		Scanner scnr = null;
		Scanner scnr2 = null;
		Scanner scnr3 = null;
		try {
			scnr = new Scanner(file);
			scnr2 = new Scanner(file);
			scnr3 = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 

		System.out.println("------***************------lecture du fichier------------------******************-------");
		lecture(scnr);

		System.out.println("-----****************-------lecture du fichier � l'envers------****************-------");
		lectureInverse(scnr2);

		lectureEcriture(scnr3);

	}
}

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AnalyseText {

	public static void main(String[] args) throws IOException {

		
		File outputFile = new File("texte2.txt");
		FileWriter out = new FileWriter(outputFile);

		BufferedReader in = new BufferedReader(new FileReader("texte1.txt"));
		String str;
		
		//stocker un mot sur deux dans un fichier texte2.txt
		while ((str = in.readLine()) != null) {
			System.out.println(str);
			List<String> words = new ArrayList<>(Arrays.asList(str.split(" ")));
			for (int i = 0; i < words.size(); i += 2) {
				//System.out.println(words.get(i));
				out.write(words.get(i) + " "); 
			}
			out.write("\n"); 
		}
		in.close();
		out.flush();
		out.close();

	}

}

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MaximumWithoutExceptions {
	public static void main(String args[]) throws IOException {
		try {
			BufferedReader br = new BufferedReader(new FileReader("data.txt"));

			try {
				int max = -1;
				String line = br.readLine();
				try {
					while (line != null) {
						int n = Integer.parseInt(line);
						// peut g�n�rer NumberFormatException

						if (n > max)
							max = n;
						try {
							line = br.readLine();
						} catch (IOException e) {
							System.out.println(e.getMessage());
						}
					}
				} catch (NumberFormatException e) {
					System.out.println(e.getMessage());
				}

				System.out.println("Maximum = " + max);

			} catch (IOException e1) {
				System.out.println(e1.getMessage());
			}
		} catch (FileNotFoundException e) {

			System.out.println(e.getMessage());
		}
	

	}
}

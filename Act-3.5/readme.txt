Vintud est un projet maven dont l'architecture est :

Vintud
|-- pom.xml
`-- /src
    |-- /main/java/com/thp/projet/vintud/

vintud.war est le war de l'application.
Pour tester vintud.war merci de le deployer sous votre serveur puis demarrer le serveur
Dans le cas de tomcat, il suffit de placer le war sous la répertoire 'webapps' ,demarrer le serveur et lancer l'url dans le navigateur. 
http://localhost:9090/vintud/Home

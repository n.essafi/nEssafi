package com.thp.project.vintud.dao.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.thp.project.vintud.dao.factory.FactoryDao;
import com.thp.project.vintud.dao.impl.AdDaoImpl;
import com.thp.project.vintud.entity.Ad;

@WebServlet("/CreateAnnouncementController")
public class CreateAnnouncementController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final FactoryDao factoryDao = new FactoryDao();

	public CreateAnnouncementController() {
		super();

	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/Create_Announcement.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String title = request.getParameter("title");
		String description = request.getParameter("description");
		String price = request.getParameter("price");
		String localisation = request.getParameter("localisation");
		
		
		  java.util.Date date = new Date(); 
		  java.sql.Timestamp currentdate = new java.sql.Timestamp(date.getTime());


		Ad anounce = new Ad(title, description, Double.parseDouble(price), localisation, currentdate);
		AdDaoImpl announceImpl = factoryDao.getAdDao();
		try {
			announceImpl.create(anounce, 1, 1);
		} catch (SQLException e) {

			e.getMessage();
		}
		
		response.sendRedirect("/vintud/Announcement");

	}

}

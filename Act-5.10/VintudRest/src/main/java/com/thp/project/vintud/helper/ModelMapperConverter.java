package com.thp.project.vintud.helper;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.springframework.stereotype.Component;

import com.thp.project.vintud.dto.*;
import com.thp.project.vintud.entity.*;

@Component
public class ModelMapperConverter {

	static ModelMapper modelMapper = new ModelMapper();

	public static <T extends MyDTO> T converToDTO(MyEntity source, Class<T> clazz) {
		return modelMapper.map(source, clazz);
	}

	public static <T extends MyEntity> T converToEntity(MyDTO source, Class<T> clazz) {
		return modelMapper.map(source, clazz);
	}

	public static <T extends MyEntity> List<T> convertAllToEntity(List<? extends MyDTO> source, Class<T> clazz) {
		List<T> entities = new ArrayList<>();
		for (MyDTO dto : source) {
			entities.add(ModelMapperConverter.converToEntity(dto, clazz));

		}
		return entities;
	}

	public static <T extends MyDTO> List<T> convertAllToDTO(List<? extends MyEntity> source, Class<T> clazz) {
		List<T> entitieDTOs = new ArrayList<>();
		for (MyEntity entity : source) {
			entitieDTOs.add(ModelMapperConverter.converToDTO(entity, clazz));

		}
		return entitieDTOs;
	}

	// user convertor
	public static UserDTO converUserToDTO(User source) {
		return modelMapper.map(source, UserDTO.class);
	}

	public static List<UserDTO> convertUserAllToDTO(List<User> source) {
		List<UserDTO> entitieDTOs = new ArrayList<>();
		for (User entity : source) {
			entitieDTOs.add(ModelMapperConverter.converUserToDTO(entity));

		}
		return entitieDTOs;
	}

	// specific DTO for Ads update idbuyer istead on entity
	public static AdDTO converAdToDTO(Ad source) {
		TypeMap<Ad, AdDTO> typeMap = modelMapper.getTypeMap(Ad.class, AdDTO.class);
		if (typeMap == null) {

			modelMapper.addMappings(new PropertyMap<Ad, AdDTO>() {
				@Override
				protected void configure() {
					map().setIdowner(source.getOwner().getIdUser());
					map().setCategory(source.getCategoryAd().getNameCategory());
				}
			});
		}

		return modelMapper.map(source, AdDTO.class);

	}

	public static List<AdDTO> convertAllAdToDTO(List<Ad> source) {
		List<AdDTO> adDTOS = new ArrayList<>();
		for (Ad entity : source) {
			adDTOS.add(ModelMapperConverter.converAdToDTO(entity));

		}
		return adDTOS;
	}

	public static Ad convertToAd(AdDTO source) {
		return modelMapper.map(source, Ad.class);
	}

	public static User convertToUser(UserDTO userDto) {
		User user = modelMapper.map(userDto, User.class);

		return user;
	}

}

package com.thp.project.vintud.dto;

import java.sql.Timestamp;

public class AdDTO extends MyDTO {

	private int idAd;
	private String titleAd;
	private String pictureAd;
	private String descriptionAd;
	private double priceAd;
	private Timestamp datePublication;
	private boolean availabilityAd;
	private String localisationAd;
	private int numberView;
	private int idowner;
	private String category;
	
	
	public int getIdowner() {
		return idowner;
	}


	public void setIdowner(int idowner) {
		this.idowner = idowner;
	}


	public void setCategory(String category) {
		this.category = category;
	}

	
	public String getCategory() {
		return category;
	}
	

	public int getIdAd() {
		return idAd;
	}

	

	public String getTitleAd() {
		return titleAd;
	}

	public String getPictureAd() {
		return pictureAd;
	}

	public String getDescriptionAd() {
		return descriptionAd;
	}

	public double getPriceAd() {
		return priceAd;
	}

	public Timestamp getDatePublication() {
		return datePublication;
	}

	public boolean isAvailabilityAd() {
		return availabilityAd;
	}

	public String getLocalisationAd() {
		return localisationAd;
	}

	public int getNumberView() {
		return numberView;
	}

	


	public void setIdAd(int idAd) {
		this.idAd = idAd;
	}

	public void setTitleAd(String titleAd) {
		this.titleAd = titleAd;
	}

	public void setPictureAd(String pictureAd) {
		this.pictureAd = pictureAd;
	}

	public void setDescriptionAd(String descriptionAd) {
		this.descriptionAd = descriptionAd;
	}

	public void setPriceAd(double priceAd) {
		this.priceAd = priceAd;
	}

	public void setDatePublication(Timestamp datePublication) {
		this.datePublication = datePublication;
	}

	public void setAvailabilityAd(boolean availabilityAd) {
		this.availabilityAd = availabilityAd;
	}

	public void setLocalisationAd(String localisationAd) {
		this.localisationAd = localisationAd;
	}

	public void setNumberView(int numberView) {
		this.numberView = numberView;
	}

	

	
	
}

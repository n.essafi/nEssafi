package com.thp.project.vintud.dto;


public class UserDTO extends MyDTO{


	private int idUser;
	private String firstName;
	private String lastName;
	private String pseudo;
	private String password;
	private String mail;
	private int phone;
	private String address;
	private String username;
	
	public UserDTO() {
		
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public int getIdUser() {
		return idUser;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getPassword() {
		return password;
	}

	public String getMail() {
		return mail;
	}

	public int getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	

}
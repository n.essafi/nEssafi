package com.thp.project.vintud.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idRole;

	@Column(name = "nom")
	private String name;

	public Role(String name) {
		this.name = name;
	}
	public Role() {
		
	}

	public Role(Roles nameRole) {
		this.name = nameRole.name();
	}

	public String getName() {
		return name;
	}

	public void setName(Roles nameRole) {
		this.name = nameRole.name();
	}

	public int getIdRole() {
		return idRole;
	}

	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}

	
}

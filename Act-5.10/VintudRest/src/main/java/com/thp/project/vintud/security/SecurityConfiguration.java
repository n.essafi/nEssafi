package com.thp.project.vintud.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.thp.project.vintud.repository.UserRepository;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
		
	    private UserPrincipalDetailsService userPrincipalDetailsService;
	    private UserRepository userRepository;

	    public SecurityConfiguration(UserPrincipalDetailsService userPrincipalDetailsService, UserRepository userRepository) {
	        
	    	this.userPrincipalDetailsService = userPrincipalDetailsService;
	        this.userRepository = userRepository;
	    }

	    @Override
	    protected void configure(AuthenticationManagerBuilder auth) {
	        auth.authenticationProvider(authenticationProvider());
	    }

	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	       
	                // remove csrf and state in session because in jwt we do not need them
	            http.csrf().disable()
	                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
	                .and()
	                // add jwt filters (1. authentication, 2. authorisation)
	                .addFilter(new JWTAuthenticationFilter(authenticationManager()))
	                .addFilter(new JwtAuthorizationFilter(authenticationManager(),  this.userRepository))
	                .authorizeRequests()
	                // configure access rules
	                .antMatchers(HttpMethod.POST, "/login").permitAll()
	                .antMatchers(HttpMethod.PUT, "/users/*").hasRole("CLIENT")
	                .antMatchers(HttpMethod.GET, "/users/admin").hasRole("ADMIN")
	                
	                .anyRequest().authenticated();
	           
	    }

	    @Bean
	    DaoAuthenticationProvider authenticationProvider(){
	        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
	        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
	        daoAuthenticationProvider.setUserDetailsService(this.userPrincipalDetailsService);

	        return daoAuthenticationProvider;
	    }

	    @Bean
	    PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }
	}


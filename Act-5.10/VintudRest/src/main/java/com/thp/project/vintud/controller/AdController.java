package com.thp.project.vintud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.thp.project.vintud.dto.AdDTO;
import com.thp.project.vintud.service.AdService;

@RestController()
@RequestMapping(path = "/ads")
@CrossOrigin
public class AdController {

	@Autowired
	AdService adserviceimpl;

	@GetMapping
	public List<AdDTO> getAllAds(@RequestParam(value="title", required=false) String title,
			@RequestParam(value="category", required=false) String category) {
		if (title != null) {
			if (category != null) {
				return adserviceimpl.findByNameAndCateg(title, category);
			} else {
				return adserviceimpl.findByName(title);
			}
		}else if (category != null){
			return adserviceimpl.findByCateg(category);
		}else {
			return adserviceimpl.findAllAds();
		}
		
	}

	@GetMapping(value = "/{userId}",
			produces = { MediaType.APPLICATION_JSON_VALUE, 
            MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody List<AdDTO> getAllAds(@PathVariable int userId) {
		return adserviceimpl.findAvailableAdsByuser(userId);
	}

}

package com.thp.project.vintud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.thp.project.vintud.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User>{

	List<User> findByMailAndPassword(String mail, String password);

	User findByUsername(String userName);

	User findByMail(String mail);
	


}

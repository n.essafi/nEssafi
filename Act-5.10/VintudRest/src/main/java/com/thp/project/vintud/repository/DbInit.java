package com.thp.project.vintud.repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.Roles;
import com.thp.project.vintud.entity.User;

@Service
public class DbInit implements CommandLineRunner {

	 private UserRepository userRepository;
	 private PasswordEncoder passwordEncoder;

	    public DbInit(UserRepository userRepository, PasswordEncoder passwordEncoder) {
	    	this.userRepository = userRepository;
	        this.passwordEncoder = passwordEncoder;
	}

		@Override
		public void run(String... args) throws Exception {

	        this.userRepository.deleteAll();

	        User client = new User(passwordEncoder.encode("root1"), "nejah","" ) ;
	        client.setRole(new Role("CLIENT"));      
	        
	        User admin = new User(passwordEncoder.encode("root2"), "yassin","ACCESS_TEST1,ACCESS_TEST2" ) ;
	        admin.setRole(new Role("ADMIN"));
	        
	        List<User> users = Arrays.asList(client,admin);
	        userRepository.saveAll(users);
		}

}



le projet one million lines contient deux fichier HTML (index.html) et de CSS (custom.css) . 
Tous les fichiers nécessaires sont fournis. 

dans ce projet on a utilisé le bootstrap 4.
le template HTML contient plusieurs parties dans l'ordre :

* une barre de navigation
* un jumbotron ayant pour id ‘home’, contenant la première partie et son contenu
* une div ayant pour id ‘who’, contenant la seconde partie et son contenu
* une div ayant pour id ‘get-involved’, contenant la troisième partie et son contenu
* un footer
* une div ayant pour id 'back-to-top’ et contenant la flèche permettant de remonter au jumbotron

Le site est responsive sur smartphone (max-width: 767px) .
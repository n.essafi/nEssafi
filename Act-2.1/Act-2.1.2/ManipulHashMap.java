import java.util.HashMap;
import java.util.Map;

public class ManipulHashMap {

	public static void main(String[] args) {
		Map<String, Integer> numberMapping = new HashMap<>();

		numberMapping.put("One", 1);
		numberMapping.put("Four", 4);
		numberMapping.put("Two", 2);
		numberMapping.put("Three", 3);

		System.out.println(numberMapping);

		System.out.println("////////////////////////////////////////////////////////////////");

		Map<String, String> cityMapping = new HashMap<>();

		cityMapping.put("Steve", "London");
		cityMapping.put("John", "New York");
		cityMapping.put("Rajeev", "Bengaluru");

		System.out.println(cityMapping);

		size(cityMapping);

		existKey(cityMapping, "Steve");

		empty(cityMapping);

		existValue(cityMapping, "New York");

		getValue(cityMapping, "Rajeev");
		
		removeEntrie (cityMapping, "Steve");
		
		removeEntrieAssocied (cityMapping, "John", "New York");

	}

	public static void existKey(Map<String, String> map, String key) {

		if (map.containsKey(key)) {

			System.out.println("la cl� " + key + " existe");
		} else {
			System.out.println("la cl� " + key + " n'existe pas");
		}

	}

	public static void empty(Map<String, String> map) {

		if (map.isEmpty()) {
			System.out.println("cityMapping est vide ");
		} else {
			System.out.println("cityMapping n'est pas vide ");
		}
	}

	public static void size(Map<String, String> map) {
		System.out.println("la taille du cityMapping " + map.size());
	}

	public static void existValue(Map<String, String> map, String value) {

		if (map.containsValue(value)) {

			System.out.println("la valeur " + value + " existe");

		} else {
			System.out.println("la valeur " + value + " n'existe pas");
		}

	}

	public static void getValue(Map<String, String> map, String key) {

		if (map.get(key) != null) {

			System.out.println("La valeur de la cle " + key + " est : " + map.get(key));

		} else {
			
			System.out.println("La cle " + key + " n'existe pas ");

		}

	}
	
	public static void removeEntrie (Map<String, String> map, String key) {
	
			map.remove(key);
			System.out.println(map);
	}
	
	public static void removeEntrieAssocied (Map<String, String> map, String key,String value) {
		
		map.remove(key,value);
		System.out.println(map);

	}

}

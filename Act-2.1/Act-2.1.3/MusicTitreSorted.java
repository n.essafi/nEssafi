import java.util.Comparator;

public class MusicTitreSorted implements Comparator<MusiqueImpl> {
	@Override
	public int compare(MusiqueImpl m1, MusiqueImpl m2) {
		if (m1.getMusicTitle().compareToIgnoreCase(m2.getMusicTitle()) < 0) {
			return -1;
		} else {
			return +1;
		}
	}
}

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class PlayListeImpl implements PlayList {

	private ArrayList<MusiqueImpl> musicPlaylist;
	private String genre;
	private String nom;

	public PlayListeImpl(String genre, String nom) {
		this.musicPlaylist = new ArrayList<>();
		this.genre = genre;
		this.nom = nom;

	}

	@Override
	public String toString() {
		return "PlayListeImpl [musicPlaylist=" + musicPlaylist + ", genre=" + genre + ", nom=" + nom + "]";
	}

	// ajouter music au playlist
	@Override
	public void addMusicToPlaylist(MusiqueImpl music) {

		this.musicPlaylist.add(music);

	}

	// supprimer les doublons
	@Override
	public void deleteDoubleMusic() {
		Set<MusiqueImpl> ListwithoutDuplicate = new LinkedHashSet(this.musicPlaylist);
		this.musicPlaylist.clear();
		this.musicPlaylist.addAll(ListwithoutDuplicate);

	}

	@Override
	public void triMusicByAuthor() {

		// Sorted by Auteur
		Collections.sort(this.musicPlaylist, new MusicAuteurSorted());
		System.out.println(this.musicPlaylist);
	}

	@Override
	public void triMusicByInterpreter() {
		// Sorted by Interpreteur
		Collections.sort(this.musicPlaylist, new MusicInterpreteurSorted());
		System.out.println(this.musicPlaylist);

	}

	// affichier les musiques commues entre deux playlist
	@Override
	public void displaySameMusic(PlayListeImpl playlist2) {

		for (int i = 0; i < this.musicPlaylist.size(); i++) {
			if (playlist2.musicPlaylist.contains(this.musicPlaylist.get(i))) {
				System.out.println(this.musicPlaylist.get(i));

			}

		}

	}

	public ArrayList<MusiqueImpl> getMusicPlaylist() {
		return musicPlaylist;
	}

	public void setMusicPlaylist(ArrayList<MusiqueImpl> musicPlaylist) {
		this.musicPlaylist = musicPlaylist;
	}

}

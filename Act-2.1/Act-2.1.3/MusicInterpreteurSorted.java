import java.util.Comparator;

public class MusicInterpreteurSorted implements Comparator<MusiqueImpl> {

	@Override
	public int compare(MusiqueImpl m1, MusiqueImpl m2) {
		if (m1.getMusicInterpreter().compareToIgnoreCase(m2.getMusicInterpreter()) < 0) {
			return -1;
		} else {
			return +1;
		}
	}
}

import java.util.ArrayList;
import java.util.Collections;

public class User {

	private ArrayList<PlayListeImpl> PlayListUser;
	private String prenom;
	private String nom;
	private String pseudo;

	public User(String prenom, String nom, String pseudo) {

		this.prenom = prenom;
		this.nom = nom;
		this.pseudo = pseudo;
		this.PlayListUser = new ArrayList<>();
	}

	// ajouter un playlist au playlistUser
	public void addPlaylisToUserPlayList(PlayListeImpl playlist) {
		this.PlayListUser.add(playlist);
	}

	@Override
	public String toString() {
		return "User [PlayListUser=" + PlayListUser + ", prenom=" + prenom + ", nom=" + nom + ", pseudo=" + pseudo
				+ "]";
	}

	// recherche par titre
	public void searchBytitre(String titre) {

		boolean isExist = false;

		for (PlayListeImpl playlist : this.PlayListUser) {

			for (MusiqueImpl music : playlist.getMusicPlaylist()) {

				if (music.getMusicTitle().equalsIgnoreCase(titre)) {

					System.out.println(music);
					isExist = true;
				}
			}
		}

		if (isExist == false) {
			System.out.println("le titre n'existe pas");
		}
	}

	// recherche par Autheur

	public void searchByAuteur(String autheur) {

		boolean isExist = false;

		for (PlayListeImpl playlist : this.PlayListUser) {

			for (MusiqueImpl music : playlist.getMusicPlaylist()) {

				if (music.getMusicAuthor().equalsIgnoreCase(autheur)) {

					System.out.println(music);

					isExist = true;
				}
			}
		}

		if (isExist == false) {
			System.out.println("le titre n'existe pas");
		}
	}

	public void triCroissantByTitre() {

		// Sorted by Auteur
		for (PlayListeImpl playlist : this.PlayListUser) {

			Collections.sort(playlist.getMusicPlaylist(), new MusicTitreSorted());
			System.out.println(this.PlayListUser);
		}

	}

	public void triDeroissantByTitre() {

		// Sorted by Auteur
		for (PlayListeImpl playlist : this.PlayListUser) {

			Collections.sort(playlist.getMusicPlaylist(), new MusicTitreSorted().reversed());
			System.out.println(this.PlayListUser);
		}

	}

	public void deleteMusic(MusiqueImpl musicToDelate) {

		for (PlayListeImpl playlist : this.PlayListUser) {

			playlist.getMusicPlaylist().removeIf(music -> music.equals(musicToDelate));

		}
		System.out.println(this.PlayListUser);

	}

}

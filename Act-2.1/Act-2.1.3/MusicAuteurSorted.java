import java.util.Comparator;

public class MusicAuteurSorted implements Comparator<MusiqueImpl> {

	@Override
	public int compare(MusiqueImpl m1, MusiqueImpl m2) {
		if (m1.getMusicAuthor().compareToIgnoreCase(m2.getMusicAuthor()) < 0) {
			return -1;
		} else {
			return +1;
		}
	}

}

import java.util.ArrayList;

public class PlayListeUI {

	public static void main(String[] args) {

		MusiqueImpl m1 = new MusiqueImpl("Before you go", " Nejah", " mahdi", " jazz");
		MusiqueImpl m2 = new MusiqueImpl("Ride it", " mohamed", " Adel", " jazz");
		MusiqueImpl m3 = new MusiqueImpl("loco contigo", " yassine", " Adam", " jazz");
		MusiqueImpl m4 = new MusiqueImpl("memories", "amina", " amir", " jazz");

		System.out.println(m1.equals(m2));

		System.out.println("****************************************************************************************");

		PlayListeImpl playlist1 = new PlayListeImpl("N", "album 1");

		playlist1.addMusicToPlaylist(m1);
		playlist1.addMusicToPlaylist(m2);
		playlist1.addMusicToPlaylist(m2);
		playlist1.addMusicToPlaylist(m4);

		System.out.println("playlist 1 : " + playlist1);

		System.out.println("*************************************************************************************");
		PlayListeImpl playlist2 = new PlayListeImpl("house", "album2");
		playlist2.addMusicToPlaylist(m1);
		playlist2.addMusicToPlaylist(m2);

		// supprimer les doublons
		playlist1.deleteDoubleMusic();
		System.out.println("List sans doublons:" + playlist1);

		System.out.println("**********display sameMusic***************************************************************");
		// afficher les doublons

		playlist1.displaySameMusic(playlist2);
		System.out.println("*************playlist1 tri� par ordre croissant par auteur **********************" );
		playlist1.triMusicByAuthor(); // trie du playliste par auteur
		
		System.out.println("*************playlist1 tri� par ordre croissant par Interpreteur ****************************" );
		
		playlist1.triMusicByInterpreter(); // trie du playliste par interpreteur
		
		
		
		System.out.println("///////////// search by title/////////////////////////////////");

		 User utilisateur = new User("Walid","benMahmoud","BMW");
		 utilisateur.addPlaylisToUserPlayList(playlist1);
		 
		
		 utilisateur.searchBytitre("Before you go");
		 
		 System.out.println("///////////// search by author/////////////////////////////////");

		 utilisateur.searchByAuteur(" mohamed");
		 
		 System.out.println("/////////////////////// triCroissantByTitre/////////////////////////////////");

		 utilisateur.triCroissantByTitre();
		 
		 System.out.println("//////////////////////// triCroissantByTitre/////////////////////////////////");

		 utilisateur.triDeroissantByTitre();

		 System.out.println("///////////// deleteMusic/////////////////////////////////");

		 
		 utilisateur.deleteMusic(m2);
		 
		 
		 
		 
	}

}


public interface Musique {

	public String toString();

	public boolean equals(Musique music);

}

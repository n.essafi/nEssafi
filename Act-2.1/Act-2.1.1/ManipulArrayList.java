import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManipulArrayList {

	public static void suppressionIndex(List<String> list, int index) {

		System.out.println("Suppression de l'�l�ment en position : " + index);
		System.out.println("Suppression de l'�l�ment:" + list.remove(index));
		System.out.println("Nouveau tableau:" + list);
		System.out.println("Taille du tableau:" + list.size());
	}

	public static void suppressionNom(List<String> list, String nom) {

		System.out.println("Suppression de l'animal (nom): " + nom);
		while (list.remove(nom)) {
		}
		System.out.println("Nouveau tableau:" + list);
		System.out.println("Taille du tableau:" + list.size());
	}

	public static void rechercheIndex(List<String> list, int index) {

		String val = list.get(index);
		System.out.println(val);

	}

	public static void rechercheNom(List<String> list, String nom) {

		if (list.contains(nom)) {
			System.out.println("l'animal " + nom + " exite � la postion :" + list.indexOf(nom));
		}
	}

	public static List<String> trieByCollectionSort(List<String> list) {
		Collections.sort(list);
		return list;
	}

	public static List<String> trieByListSort(List<String> list) {
		list.sort(null);
		return list;
	}

	public static List<String> customedTrie(List<String> list) {
		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(i).compareToIgnoreCase(list.get(j)) > 0) {
					Collections.swap(list, i, j);
				}
			}
		}
		return list;
	}

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<>();
		list.add("Lion");
		list.add("Tiger");
		list.add("Cat");
		list.add("Dog");

		System.out.println(list);

		list.add(2, "Elephant");

		System.out.println(list);

		System.out.println("////////////////////////////////////////////////////////////////");
		ArrayList<String> copyList = new ArrayList<>(list);
		System.out.println(copyList);

		list.addAll(copyList);
		System.out.println(list);
		System.out.println("Taille du tableau :" + list.size());

		System.out.println("////////////////////////////////////////////////////////////////");
		suppressionIndex(list, 4);

		System.out.println("////////////////////////////////////////////////////////////////");
		suppressionNom(list, "Lion");

		System.out.println("///////////////////////////////////////////////////////////////");
		rechercheIndex(list, 1);

		System.out.println("///////////////////////////////////////////////////////////////");
		rechercheNom(list, "Cat");

		System.out.println("//////////////trie using Collections.sort/////////////////////");

		System.out.println(trieByCollectionSort(list));

		System.out.println("//////////////trie using ArrayList.sort/////////////////////");

		System.out.println(trieByListSort(list));

		System.out.println("//////////////trie using Custom comparator/////////////////////");

		System.out.println(customedTrie(list));

	}

}

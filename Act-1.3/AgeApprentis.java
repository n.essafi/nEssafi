public class AgeApprentis {

	public static void main(String[] args) {

		int[] ageArray = { 45, 54, 23, 32, 64, 46, 28, 82 };

		for (int i = ageArray.length - 1; i > 0; i--) {

			int minIndex = 0;

			for (int j = 1; j <= i; j++) {

				if (ageArray[j] < ageArray[minIndex]) {

					minIndex = j;
				}
			}

			swap(ageArray, minIndex, i);
		}

		for (int val : ageArray) {
			System.out.println(val);
		}

	}

	public static void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}

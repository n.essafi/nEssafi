public class OrientationEtoile {

	public static void main(String[] args) {

		String[] etoil = { "Sirius", "Rigel", "Betelgeuse", "Arcturus", "Aldebaran", "Vega", "Deneb", "Altaer",
				null, null };

		System.out.println("le parcours dans le tableau non trié :");
		for (String val : etoil) {
			System.out.println(val);
		}
		trie(etoil);
		System.out.println("le parcours dans le tableau apres tri dans l'ordre croissant :");

		for (String val : etoil) {
			System.out.println(val);
		}

		shiftAndInsert(etoil, "dener"); // insertion du premier mot
		shiftAndInsert(etoil, "sonia"); // insertion du deuxieme mot

		System.out.println(" le parcour du tableau apres l'insertion :");

		for (int i = 0; i < etoil.length; i++) { //
			System.out.println(etoil[i]);
		}
		searchDichotomique (etoil, "sonia"); // retourne l'index du mot recherch� si valide

	}

	/*
	 * serachINsertPos : fonction qui retourne la position de l'element � inserer
	 * input : valeur � inserrer 
	 * output : position d'insertion 
	 */
	
	public static int serachINsertPos(String[] array, String val) { 

		int lastInsertedInex = nbrOfInsertedElem(array) - 1;

		int pos = 0;

		while (pos <= lastInsertedInex && array[pos].compareToIgnoreCase(val) < 0) {

			pos++;

		}
		return pos;

	}

	/*
	 * shiftAndInsert : permet de decaler et inserer 
	 * input : tableau avant insertion et la valeur � inserrer 
	 * output : le tableau apres insertion
	 */

	public static String[] shiftAndInsert(String[] array, String insertVal) {

		int n = nbrOfInsertedElem(array);
		int position = serachINsertPos(array, insertVal);
		int lastInsertedInex = n - 1;

		while (lastInsertedInex < array.length - 1 && lastInsertedInex >= position) {

			array[lastInsertedInex + 1] = array[lastInsertedInex];
			lastInsertedInex--;
		}
		array[position] = insertVal;

		return array;
	}
	
	
	/*
	 * fonction search : elle permet de faire la recherche dicothomique
	 * input : tableau , valeur
	 * output: l'index si la valeur existe 
	 */

	public static void searchDichotomique (String[] array, String searchedVal) {

		int indexSearch = -1;
		int min = 0;
		int max = nbrOfInsertedElem(array) - 1; 
		boolean test = false;

		do {
			int m = (max + min) / 2; 
			if (array[m].equalsIgnoreCase(searchedVal)) {
				indexSearch = m;
				test = true;
			} else if (array[m].compareToIgnoreCase(searchedVal) < 0) { 
				min = m + 1;
			} else { 
				max = m - 1;
			}

		} while (test == false && min <= max);
		if (test) {
			System.out.println(
					"le nom " + searchedVal + " que vous cherchez est valide et se trouve a l'index : " + indexSearch);
		} else {
			System.out.println("le nom " + searchedVal + " que vous cherchez n'est pas valide");
		}
	}
	
	
	
	

	/*
	 * fonction nbrOfInsertedElem: permet de calculer la taille du tableau rempli 
	 * (dont les element different de null)
	 * 
	 */
	
	public static int nbrOfInsertedElem(String[] array) { 
		
		int numberInsertedElement = 0;
		
		while (numberInsertedElement < array.length && array[numberInsertedElement] != null) { 
			
			numberInsertedElement++; 
		}
		return numberInsertedElement;
	}
	
	
	


	/*
	 * fonction trie :permet de trier le tableau dans l'ordre croissant
	 */
	
	
	public static String[] trie(String[] array) { 

		int lastInsertedIndex = nbrOfInsertedElem(array); 

		for (int i = lastInsertedIndex - 1; i > 0; i--) {

			for (int j = 0; j < i; j++) {

				if (array[j].compareToIgnoreCase(array[j + 1]) > 0) {
					String temp = array[j];
					array[j] = array[j+1];
					array[j+1] = temp;			
				}
			}
		}
		return array;
	}


}
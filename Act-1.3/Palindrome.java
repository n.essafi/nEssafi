import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {
        System.out.println("Entrer un mot : ");
        Scanner sc =new Scanner(System.in);
        String mot = sc.nextLine().toLowerCase();
        isPalindrome (mot);
        

    }
      public static void isPalindrome (String str){
            int lastIndex = str.length()/2;
            boolean egaux =true;  
            int i = 0;
            while ( i <= lastIndex && egaux == true){
                egaux = false;
                if (str.charAt(i) == str.charAt(str.length()-1-i)){
                    egaux = true;
                
                i++;
                    
                }
                
            }
            if (egaux) {
                System.out.println(str + " est palindrome");
            }else {
                System.out.println(str + " n'est pas un palindrome");
            }


         }

}
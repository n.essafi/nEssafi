public abstract class Predateur implements PredateurAction {

	private String sexe;
	private Age categorieAge;
	private int force;
	private boolean enGroupe = true;

	public Predateur(String sexe, Age categorieAge, int force) {
		super();
		this.sexe = sexe;
		this.categorieAge = categorieAge;
		this.force = force;

	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public Age getCategorieAge() {
		return categorieAge;
	}

	public void setCategorieAge(Age categorieAge) {
		this.categorieAge = categorieAge;
	}

	public int getForce() {
		return force;
	}

	public void setForce(int force) {
		this.force = force;
	}

	public boolean isEnGroupe() {
		return enGroupe;
	}

	public void setEnGroupe(boolean enGroupe) {
		this.enGroupe = enGroupe;
	}

	@Override
	public void courir() {
		switch (this.getCategorieAge()) {

		case JEUNE:
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapaciteDECourir());
			break;

		case ADULTE:
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapaciteDECourir());
			break;

		default:
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapaciteDECourir());
			break;
		}

	}

	@Override
	public void seReproduire() {
		switch (this.getCategorieAge()) {

		case JEUNE:
			System.out
					.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapaciteDEProduction());
			break;

		case ADULTE:
			System.out
					.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapaciteDEProduction());
			break;

		default:
			System.out
					.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapaciteDEProduction());
			break;
		}
	}

	
	
	
	
	


}



public class Lion extends Predateur implements LionAction {

	private boolean isDominant;
	private Domination categorieDomination;
	private boolean isImpetuosite;
	private boolean dort;
	private boolean malade;

	public Lion(String sexe, Age categorieAge, int force, boolean isDominant, Domination categorieDomination,
			boolean isImpetuosite, boolean dort, boolean malade) {
		super(sexe, categorieAge, force);
		this.isDominant = isDominant;
		this.categorieDomination = categorieDomination;
		this.isImpetuosite = isImpetuosite;
		this.dort = dort;
		this.malade = malade;
	}

	@Override
	public void nourrir() {
		System.out.println(this.getClass().getSimpleName() + " se nourrit uniquement de viande");

	}

	@Override
	public void chassser() {

		if (this.getCategorieAge() == Age.JEUNE) {
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapacitChasse());
		} else {
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapacitChasse()
					+ " et il chasse en groupe ");
		}

	}

	@Override
	public void emettreSon() {

		if (this.isEnGroupe()) {
			System.out.println(
					this.getClass().getSimpleName() + " " + "rugit pour se communiquer avec les autres lions.");
		} else {
			System.out.println(this.getClass().getSimpleName() + " " + " est seul, il ne rugie pas.");
		}
	}

	@Override
	public void entendreSon() {

		if (this.malade && this.dort)

		{
			System.out.println(this.getClass().getSimpleName() + " " + " entend tr�s bien.");
		} else {
			System.out.println(this.getClass().getSimpleName() + " " + "Il n'entend pas.");
		}

	}

	@Override
	public void seSeparer() {

		setEnGroupe(false); // actualis� le status devient seul

		System.out.println(this.getClass().getSimpleName() + " " + "  est seul, il s'est s�par� de sa troupe.");

	}

	@Override
	public void seRegroupe() {

		setEnGroupe(true); // actualis� le status devient engroupe

		System.out.println(this.getClass().getSimpleName() + " " + "  se rgroupe avec les autre .");

	}

	public boolean isDominant() {
		return isDominant;
	}

	public void setDominant(boolean isDominant) {
		this.isDominant = isDominant;
	}

	public Domination getCategorieDomination() {
		return categorieDomination;
	}

	public void setCategorieDomination(Domination categorieDomination) {
		this.categorieDomination = categorieDomination;
	}

	public boolean isImpetuosite() {
		return isImpetuosite;
	}

	public void setImpetuosite(boolean isImpetuosite) {
		this.isImpetuosite = isImpetuosite;
	}

	public boolean isMalade() {
		return malade;
	}

	public void setMalade(boolean malade) {
		this.malade = malade;
	}

	public boolean isDort() {
		return dort;
	}

	public void setDort(boolean dort) {
		this.dort = dort;
	}

	@Override
	public String toString() {
		return "Lion [isDominant=" + isDominant + ", categorieDomination=" + categorieDomination + ", isImpetuosite="
				+ isImpetuosite + ", dort=" + dort + ", malade=" + malade + "]";
	}

}

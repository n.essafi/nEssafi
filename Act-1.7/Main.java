

public class Main {

	public static void main(String[] args) {

		Lion l1 = new Lion("male", Age.ADULTE, 10, true, Domination.ALPHA, true, false, false);

		Lion l2 = new Lion("femele", Age.JEUNE, 20, false, Domination.BETA, true, true, true);

		System.out.println("**********************************************");
		System.out.println(l1);
		System.out.println("**********************************************");

		l1.chassser();
		l1.courir();
		l1.emettreSon();
		l1.entendreSon();
		l1.seReproduire();

		System.out.println("**********************************************");
		System.out.println(l2);
		System.out.println("**********************************************");

		l2.chassser();
		l2.courir();
		l2.emettreSon();
		l2.entendreSon();
		l2.seReproduire();

		Ours O1 = new Ours("male", Age.VIEUX, 20, 10, "fort", true);
		System.out.println("**********************************************");
		System.out.println(O1);
		System.out.println("**********************************************");

		O1.chassser();
		O1.courir();
		O1.emettreSon();
		O1.seReproduire();

		System.out.println("**********************************************");
		System.out.println("**********************************************");

		Predateur p1 = new Ours("male", Age.VIEUX, 20, 10, "fort", true);
		Predateur p2 = new Lion("male", Age.ADULTE, 10, true, Domination.ALPHA, true, false, false);

		Predateur[] animals = { p1, p2 };
		for (Predateur animal : animals) {
			animal.courir();
		}
	}

}



public class Ours extends Predateur implements OursAction {

	private int facteurAgressivite;
	private String puissance;
	private boolean isEnHivernation;

	public Ours(String sexe, Age categorieAge, int force, int facteurAgressivite, String puissance,
			boolean isEnHivernation) {
		super(sexe, categorieAge, force);
		this.facteurAgressivite = facteurAgressivite;
		this.puissance = puissance;
		this.isEnHivernation = isEnHivernation;
	}

	@Override
	public void nourrir() {
		System.out.println(this.getClass().getSimpleName() + " " + "L'ours se nourrit de tout puisqu'il est omnivore.");
	}

	@Override
	public void chassser() {

		if (this.getCategorieAge() == Age.JEUNE) {
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapacitChasse());
		} else {
			System.out.println(this.getClass().getSimpleName() + " " + this.getCategorieAge().getCapacitChasse()
					+ " et il chasse en solitaire ");
		}
	}

	@Override
	public void emettreSon() {
		System.out.println("L'ours hurle.");

	}

	@Override
	public void grimper() {

		System.out.print("L'ours grimpe.");

	}

	@Override
	public void creuser() {
		System.out.println("Il creuse des trous.");

	}

	@Override
	public void hiverner() {
		System.out.println("Il hiverne en hiver.");

	}

	public int getFacteurAgressivite() {
		return facteurAgressivite;
	}

	public void setFacteurAgressivite(int facteurAgressivite) {
		this.facteurAgressivite = facteurAgressivite;
	}

	public String getPuissance() {
		return puissance;
	}

	public void setPuissance(String puissance) {
		this.puissance = puissance;
	}

	public boolean isEnHivernation() {
		return isEnHivernation;
	}

	public void setEnHivernation(boolean isEnHivernation) {
		this.isEnHivernation = isEnHivernation;
	}

	@Override
	public String toString() {
		return "Ours [facteurAgressivite=" + facteurAgressivite + ", puissance=" + puissance + ", isEnHivernation="
				+ isEnHivernation + "]";
	}

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  fullName = '';
  mail = '';
  password = '';
  isLoadingResults = false;
  

  constructor(
    private formBuilder: FormBuilder, 
    private router: Router, 
    private authService: AuthService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      'username' :[null, Validators.required],
      'fullName' : [null, Validators.required],
      'mail' : [null, Validators.required],
      'password' : [null, Validators.required]
      });
    

  }

  onFormSubmit(form: NgForm) {
    console.log(form);
    this.authService.register(form)
      .subscribe(res => {
        this.router.navigate(['login']);
      }, (err) => {
        console.log(err);
        alert(err.error);
      });
  }



}

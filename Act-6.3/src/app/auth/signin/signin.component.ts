import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  SignInForm: FormGroup;
  userName = '';
  password = '';
  isLoadingResults = false;

  constructor(
    private formBuilder: FormBuilder, 
    private router: Router, 
    private authService: AuthService) { }

  ngOnInit() {
    this.SignInForm = this.formBuilder.group({
      'username' :[null, Validators.required],
      'password' : [null, Validators.required]
    });

  }

  onFormSubmit(form: NgForm) {
    console.log(form);
    this.authService.login(form)
      .subscribe(res => {
        console.log("res"+res.headers.get('Authorization'));
        if (res) {
          localStorage.setItem('token', res.headers.get('Authorization'));
          this.router.navigate(['ads']);
        }
      }, (err) => {
        console.log(err);
      });
  
}
}

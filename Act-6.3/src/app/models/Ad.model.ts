export class Ad{
  
    constructor(public isAvailable:boolean,public publicationDate:Date,
        public description:string,public localisation:string,public price:number,
        public	title:string){}

}
import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private router: Router) { }

         
intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = localStorage.getItem('token'); 
  
    if (token) {                                 
      request = request.clone({                  //2
        setHeaders: {
          'Authorization': 'Bearer ' + token     //2
        }
      });
    }
    if (!request.headers.has('Content-Type')) { //3
      request = request.clone({                 //3
        setHeaders: {                           //3
          'content-type': 'application/json'    //3
        }
      });
    }
    request = request.clone({                   //4
      headers: request.headers.set('Accept', 'application/json') //4
    });
    
    return next.handle(request).pipe(           //5
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('event--->>>', event);
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => { //5
        console.log(error);
        if (error.status === 401) {
          this.router.navigate(['login']);       //5
        }
        if (error.status === 400) {
          alert(error.error);                    //5
        }
        return throwError(error);
      }));
}
}

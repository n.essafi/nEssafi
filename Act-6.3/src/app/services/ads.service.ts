import { Injectable } from '@angular/core';
import { Ad } from '../models/Ad.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const apiUrl = 'http://localhost:9191/';
@Injectable({
  providedIn: 'root'
})
export class AdsService {

  ads : Ad[];
  adSubject = new Subject<Ad[]>();

  constructor(private http: HttpClient) { }

  emitAds(){
    this.adSubject.next(this.ads);
  }

  getAds(): Observable<Ad[]> {
    return this.http.get<Ad[]>(apiUrl + 'ads')
      .pipe(
        tap(_ => this.log('fetched Ads')),
        catchError(this.handleError('getAds', []))
      );
  }

  

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(message);
  }



}

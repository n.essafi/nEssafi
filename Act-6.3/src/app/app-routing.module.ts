import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
import { AdListComponent } from './ad-list/ad-list.component';
import { AdFormComponent } from './ad-form/ad-form.component';

const routes: Routes = [
  {path: 'register', component:SignupComponent, data:{title:'Register'}},
{path: 'login', component:SigninComponent, data:{title:'Login'}},
{path: 'ads', component:AdListComponent},
{path: 'ads/new', component: AdFormComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

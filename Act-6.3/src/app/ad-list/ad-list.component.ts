import { Component, OnInit } from '@angular/core';
import { Ad } from '../models/Ad.model';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { AdsService } from 'src/app/services/ads.service';


@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.scss']
})
export class AdListComponent implements OnInit {


  data: Ad[] = [];
displayedColumns: string[] = ['Title', 'Description','Price','Date Publication ','Localisation', 'Number View'];
isLoadingResults = true;
SignInForm: FormGroup;


  constructor(private AdsService: AdsService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.getAds();
  }


  getAds(): void {
    this.AdsService.getAds()
      .subscribe(ads => {
        this.data = ads;
        console.log(this.data);
        this.isLoadingResults = false;
      }, err => {
        console.log(err);
        this.isLoadingResults = false;
      });
  }
  
}

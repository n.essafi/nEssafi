package com.thp.simplecontext.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.thp.simplecontext.dto.MoussaillonDto;
import com.thp.simplecontext.service.impl.MoussaillonServiceImpl;

@RestController
public class MoussaillonController {

	@Autowired
	MoussaillonServiceImpl ms;

	@GetMapping(path = "/moussaillon/{moussaillonId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody MoussaillonDto getMoussaillonByIdRest(@PathVariable int moussaillonId) {

		return ms.getById(moussaillonId);

	}

	@GetMapping("/moussaillon")
	public List<MoussaillonDto> getAll() {
		return ms.display();
	}
}

package com.thp.simplecontext.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.simplecontext.dto.MoussaillonDto;
import com.thp.simplecontext.entity.Moussaillon;

@Repository
public interface MoussaillonRepository extends JpaRepository<Moussaillon, Integer> {

	Optional<Moussaillon> findByIdMoussaillon(int idMousaillon);

}

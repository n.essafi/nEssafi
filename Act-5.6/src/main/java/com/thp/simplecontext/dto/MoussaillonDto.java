package com.thp.simplecontext.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.thp.simplecontext.entity.Bateau;

public class MoussaillonDto {
	
	
	
	@JsonProperty(value="idMoussaillon")
	private int idMoussaillon ;

	@JsonProperty(value="firstName")
	private String firstName ;
	

	@JsonProperty(value="lastName")
	private String lastName ;

	@JsonProperty(value="config")
	private String config ;

	@JsonProperty(value="bateau")
	private Bateau bateau ;
	
	
	public MoussaillonDto() {

	}	
	
	public int getIdMoussaillon() {
		return idMoussaillon;
	}



	public void setIdMoussaillon(int idMoussaillon) {
		this.idMoussaillon = idMoussaillon;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public Bateau getBateau() {
		return bateau;
	}

	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}

}

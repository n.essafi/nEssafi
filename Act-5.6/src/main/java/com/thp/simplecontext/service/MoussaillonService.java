package com.thp.simplecontext.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.thp.simplecontext.dto.MoussaillonDto;
import com.thp.simplecontext.entity.Moussaillon;


@Repository
public interface MoussaillonService {

	Moussaillon create(Moussaillon moussaillon);

	Moussaillon update(Moussaillon moussaillon, String firstName, String lastName, String config);

	void delete(Moussaillon moussaillon);


	List<Moussaillon> findAllByBateau(int bateauId);

	MoussaillonDto getById(int idMousaillon);

	List<MoussaillonDto> display();
}


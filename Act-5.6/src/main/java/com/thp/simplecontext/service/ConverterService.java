package com.thp.simplecontext.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.thp.simplecontext.dto.MoussaillonDto;
import com.thp.simplecontext.entity.Moussaillon;

@Component
public class ConverterService {
	
	@Autowired
	private ModelMapper modelMapper;
	public MoussaillonDto  convertToDto(Moussaillon moussaillonObject) {
		return modelMapper.map(moussaillonObject,MoussaillonDto.class );
	}

}

package com.thp.simplecontext.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thp.simplecontext.entity.Bateau;
import com.thp.simplecontext.entity.Moussaillon;

@Repository
public interface BateauService {

	Bateau create(Bateau bateau);

	Bateau update(Bateau bateau, String name, String type, double taille);

	void delete(Bateau bateau);

	Bateau findById(int id);

	Bateau findByMoussaillon(Moussaillon moussaillon);

	List<Bateau> findAll();

}

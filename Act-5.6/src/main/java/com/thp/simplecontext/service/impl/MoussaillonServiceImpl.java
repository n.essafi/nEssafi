package com.thp.simplecontext.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.simplecontext.dto.MoussaillonDto;
import com.thp.simplecontext.entity.Moussaillon;
import com.thp.simplecontext.repository.MoussaillonRepository;
import com.thp.simplecontext.service.ConverterService;
import com.thp.simplecontext.service.MoussaillonService;

@Service
public class MoussaillonServiceImpl implements MoussaillonService {

	@Autowired
	private MoussaillonRepository moussaillonRepository;
	
	@Autowired
	private ConverterService converterService;

	@Override
	public Moussaillon create(Moussaillon moussaillon) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Moussaillon update(Moussaillon moussaillon, String firstName, String lastName, String config) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Moussaillon moussaillon) {
		// TODO Auto-generated method stub

	}

	@Override
	public MoussaillonDto getById(int idMousaillon) {
		Optional<Moussaillon> mous= moussaillonRepository.findByIdMoussaillon(idMousaillon);
		
		Moussaillon moussaillon = mous.get();
		return converterService.convertToDto(moussaillon);
	}

	@Override
	public List<Moussaillon> findAllByBateau(int bateauId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MoussaillonDto> display() {

		return moussaillonRepository.findAll().stream().map(converterService::convertToDto).collect(Collectors.toList());
	}

}

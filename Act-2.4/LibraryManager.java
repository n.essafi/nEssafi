
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class LibraryManager {

	// Une m�thode permettant d'afficher les livres de la biblioth�que pass�e en param�tre

	public static List<Book> displayBooks(String libraryName) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Book> books = new ArrayList<>();
		try {
			con = ConnectionManager.getConnection();
			if (con == null) {
				System.out.println("Error getting the connection. Please check if the DB server is running");
				return books;
			}
			ps = con.prepareStatement("select * from book B join library L on B.library_id = L.id where L.lib_name = '" +libraryName + "'");

			rs = ps.executeQuery();
			System.out.println("retrive => " + ps.toString());
			while (rs.next()) {
				Book book = new Book();
				book.setIdBook(rs.getInt("id"));
				book.setTitleBook(rs.getString("title"));
				book.setAuthorBook(rs.getString("author"));
				book.setEditorBook(rs.getString("editor"));
				book.setSummaryBook(rs.getString("summary"));
				book.setPageNbBook(rs.getInt("page_nb"));
				Library library = new Library();
				library.setIdLibrary(rs.getInt("library_id"));
				library.setNameLibrary(libraryName);
				book.setLibrary(library);
				books.add(book);

			}

		} catch (SQLException e) {
			throw e;
		}

		finally {
			try {
				ConnectionManager.closeResultSet(rs);
				ConnectionManager.closePrepaerdStatement(ps);
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {
				throw e;
			}
		}
		return books;
	}

	
	
	
	
	
	// Une m�thode permettant d'ajouter un livre � la biblioth�que 
	
	
	public static Book addBook(Book book) {

		Connection con = null;
		int lastKey = 1;
		try {
			con = ConnectionManager.getConnection();
			String query = "INSERT INTO book (title,author, editor, summary, page_nb, library_id) VALUES ( '"
					+ book.getTitleBook() + "','" + book.getAuthorBook() + "' , '" + book.getEditorBook() + "' , '"
					+ book.getSummaryBook() + "' ," + book.getPageNbBook() + "," + book.getLibrary().getIdLibrary()
					+ ")";

			Statement stmt = con.createStatement();
			System.out.println(query);
			stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

			ResultSet keys = stmt.getGeneratedKeys();

			while (keys.next()) {
				lastKey = keys.getInt(1);
			}
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			try {
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		book.setIdBook(lastKey);
		return book;
	}

	
	
	
	
//  sorter les livres par auteur
	
	public static List<Book> SortBooksbyAuthor() throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Book> books = new ArrayList<>();
		try {
			con = ConnectionManager.getConnection();
			if (con == null) {
				System.out.println("Error getting the connection. Please check if the DB server is running");
				return books;
			}
			ps = con.prepareStatement("select * from book B ORDER BY author ASC");

			rs = ps.executeQuery();
			while (rs.next()) {
				Book book = new Book();
				book.setIdBook(rs.getInt("id"));
				book.setTitleBook(rs.getString("title"));
				book.setAuthorBook(rs.getString("author"));
				book.setEditorBook(rs.getString("editor"));
				book.setSummaryBook(rs.getString("summary"));
				book.setPageNbBook(rs.getInt("page_nb"));
				Library library = new Library();
				library.setIdLibrary(rs.getInt("library_id"));
				book.setLibrary(library);
				books.add(book);

			}

		} catch (SQLException e) {
			throw e;
		}

		finally {
			try {
				ConnectionManager.closeResultSet(rs);
				ConnectionManager.closePrepaerdStatement(ps);
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {
				throw e;
			}
		}
		return books;
	}
	
	
	
	
	
	

	// Une m�thode permettant de rechercher un livre par son ID;

	public static Book SearchBookById(int bookId) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Book book = new Book();

		try {
			con = ConnectionManager.getConnection();
			ps = con.prepareStatement("select * from book WHERE id = " + bookId);
			rs = ps.executeQuery();

			while (rs.next()) {
				book.setIdBook(rs.getInt("id"));
				book.setTitleBook(rs.getString("title"));
				book.setAuthorBook(rs.getString("author"));
				book.setEditorBook(rs.getString("editor"));
				book.setSummaryBook(rs.getString("summary"));
				book.setPageNbBook(rs.getInt("page_nb"));
				Library library = new Library();
				library.setIdLibrary(rs.getInt("library_id"));
				book.setLibrary(library);
			}

		} catch (SQLException e) {
			throw e;
		}

		finally {
			try {
				ConnectionManager.closeResultSet(rs);
				ConnectionManager.closePrepaerdStatement(ps);
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {
				throw e;
			}
		}

		return book;
	}

	
	
	
	
	
// permet de rechercher des livres par auteur

	public static List<Book> searchBookByAUthor(String auteur) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Book> books = new ArrayList<>();
		try {
			con = ConnectionManager.getConnection();
			if (con == null) {
				System.out.println("Error getting the connection. Please check if the DB server is running");
				return books;
			}
			ps = con.prepareStatement("select * from book  where author = '" + auteur + "'");

			rs = ps.executeQuery();

			while (rs.next()) {
				Book book = new Book();
				book.setIdBook(rs.getInt("id"));
				book.setTitleBook(rs.getString("title"));
				book.setAuthorBook(rs.getString("author"));
				book.setEditorBook(rs.getString("editor"));
				book.setSummaryBook(rs.getString("summary"));
				book.setPageNbBook(rs.getInt("page_nb"));
				Library library = new Library();
				library.setIdLibrary(rs.getInt("library_id"));
				book.setLibrary(library);
				books.add(book);

			}

		} catch (SQLException e) {
			throw e;
		}

		finally {
			try {
				ConnectionManager.closeResultSet(rs);
				ConnectionManager.closePrepaerdStatement(ps);
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {
				throw e;
			}
		}
		return books;
	}

	
	
	
	
//	Une m�thode permettant de rechercher un utilisateur ;

	public static User searchUserById(int entredId) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = new User();

		try {
			con = ConnectionManager.getConnection();
			ps = con.prepareStatement("select * from user WHERE id = " + entredId);
			rs = ps.executeQuery();
			while (rs.next()) {
				user.setPrenomUser(rs.getString("prenom"));
				user.setNomUser(rs.getString("nom"));
				user.setAdressUser(rs.getString("addresse"));
				user.setMailUser(rs.getString("mail"));
				user.setNumTelUser(rs.getInt("numTel"));
				user.setIdUser(entredId);

			}

		} catch (SQLException e) {
			throw e;
		}

		finally {
			try {
				ConnectionManager.closeResultSet(rs);
				ConnectionManager.closePrepaerdStatement(ps);
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {
				throw e;
			}
		}

		return user;

	}

	
	
	
	
	
	
	
	// Une m�thode permettant de cr�er un utilisateur

	public static User addUser(User userToAdd) {

		Connection con = null;
		int lastKey = 1;
		try {
			con = ConnectionManager.getConnection();
			String query = "INSERT INTO user (prenom, nom, addresse, numTel, mail) VALUES ('"
					+ userToAdd.getPrenomUser() + "' , '" + userToAdd.getNomUser() + "' , '" + userToAdd.getAdressUser()
					+ "' , " + userToAdd.getNumTelUser() + " , '" + userToAdd.getMailUser() + "')";

			Statement stmt = con.createStatement();
			System.out.println(query);
			stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

			ResultSet keys = stmt.getGeneratedKeys();

			while (keys.next()) {
				lastKey = keys.getInt(1);
			}

			userToAdd.setIdUser(lastKey);
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			try {
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}
		return userToAdd;
	}

	
	
	
	
	
//	Une m�thode permettant de cr�er un pr�t
	public static Rent createRent(Rent newRent) {

		Connection con = null;
		int lastKey = 1;
		try {
			con = ConnectionManager.getConnection();
			String query = "INSERT INTO rent (date_pret, date_fin, user_id, book_id) VALUES ('" + newRent.getDatePret()
					+ "' ,' " + newRent.getDateFin() + "' ," + newRent.getUserRent().getIdUser() + ","
					+ newRent.getUserBook().getIdBook() + ")";
			Statement stmt = con.createStatement();
			System.out.println(query);
			stmt.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);

			ResultSet keys = stmt.getGeneratedKeys();

			while (keys.next()) {
				lastKey = keys.getInt(1);
			}

			newRent.setIdRent(lastKey);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {

			try {
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {

				e.printStackTrace();
			}
		}

		return newRent;
	}
	
	
	
	
	
	

//	Une m�thode permettant de trier tous les pr�ts par date de fin d�croissante d'une library pass�e en param�tre

	public static List<Rent> sortRentByDate(String libraryName) throws SQLException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Rent> rents = new ArrayList<>();
		try {
			con = ConnectionManager.getConnection();
			if (con == null) {
				System.out.println("Error getting the connection. Please check if the DB server is running");
				return rents;
			}
			ps = con.prepareStatement("select rent.id , date_fin  from rent join book ON rent.book_id = book.id"+
					 " join library ON book.library_id = library.id" + " where library.lib_name = '" + libraryName + "'"
					+ "ORDER BY date_fin ");

			rs = ps.executeQuery();
			while (rs.next()) {
				Rent rent = new Rent();
				rent.setIdRent(rs.getInt("id"));
				rent.setDateFin(rs.getTimestamp("date_fin"));
				
				rents.add(rent);

			}

		} catch (SQLException e) {
			throw e;
		}

		finally {
			try {
				ConnectionManager.closeResultSet(rs);
				ConnectionManager.closePrepaerdStatement(ps);
				ConnectionManager.closeConnection(con);
			} catch (SQLException e) {
				throw e;
			}
		}
		return rents;

	}

}

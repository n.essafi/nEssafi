

import java.sql.Timestamp;
import java.util.Date;

public class Rent {

	private int idRent;
	private User userRent;
	private Book userBook;
	private Timestamp DatePret, DateFin;

	public Rent( Timestamp datePret,Timestamp dateFin, User userRent, Book userBook) {
		
		
		this.userRent = userRent;
		this.userBook = userBook;
		this.DatePret = datePret;
		this.DateFin = dateFin;
	}
	

public Rent() {
		
	}


	// afficher Rent
	@Override
	public String toString() {
		return "le rent n� " + idRent + " : l'utilisateur n� " + userRent.getIdUser() + " a pr�t� le livre n� " + userBook.getIdBook() + " � partir du " + DatePret
				+ " jusqu'� " + DateFin ;
	}

// comparer deux Rents
	public boolean equals(Rent obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rent other = (Rent) obj;
		if (DateFin == null) {
			if (other.DateFin != null)
				return false;
		} else if (!DateFin.equals(other.DateFin))
			return false;
		if (DatePret == null) {
			if (other.DatePret != null)
				return false;
		} else if (!DatePret.equals(other.DatePret))
			return false;
		if (userBook == null) {
			if (other.userBook != null)
				return false;
		} else if (!userBook.equals(other.userBook))
			return false;
		return true;
	}

//getter and setter
	public int getIdRent() {
		return idRent;
	}

	public void setIdRent(int idRent) {
		this.idRent = idRent;
	}

	public User getUserRent() {
		return userRent;
	}

	public void setUserRent(User userRent) {
		this.userRent = userRent;
	}

	public Book getUserBook() {
		return userBook;
	}

	public void setUserBook(Book book) {
		userBook = book;
	}

	public Timestamp getDatePret() {
		return DatePret;
	}

	public void setDatePret(Timestamp datePret) {
		DatePret = datePret;
	}

	public Timestamp getDateFin() {
		return DateFin;
	}

	public void setDateFin(Timestamp dateFin) {
		DateFin = dateFin;
	}

}

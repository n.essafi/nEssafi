

import java.util.ArrayList;

public class User {

	private int idUser;
	private String prenomUser;
	private String nomUser;
	private String adressUser;
	private int numTelUser;
	private String mailUser;
	private ArrayList<Rent> userRents;

	public User( String prenomUser, String nomUser, String adressUser, int numTelUser, String mailUser) {
		super();
		
		this.prenomUser = prenomUser;
		this.nomUser = nomUser;
		this.adressUser = adressUser;
		this.numTelUser = numTelUser;
		this.mailUser = mailUser;

	}

	public User() {

	}

	// Affichage User
	@Override
	public String toString() {
		return "Le user n� " + idUser + " s'appelle " + prenomUser +" "+ nomUser + " habite � "
				+ adressUser + ", son numero de telephone est :" + numTelUser + " et son mail est :"  + mailUser ;
	}

// comparer deux user

	public boolean equals(User obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (adressUser == null) {
			if (other.adressUser != null)
				return false;
		} else if (!adressUser.equals(other.adressUser))
			return false;
		if (idUser != other.idUser)
			return false;
		if (mailUser == null) {
			if (other.mailUser != null)
				return false;
		} else if (!mailUser.equals(other.mailUser))
			return false;
		if (nomUser == null) {
			if (other.nomUser != null)
				return false;
		} else if (!nomUser.equals(other.nomUser))
			return false;
		if (numTelUser != other.numTelUser)
			return false;
		if (prenomUser == null) {
			if (other.prenomUser != null)
				return false;
		} else if (!prenomUser.equals(other.prenomUser))
			return false;
		return true;
	}

//getter

	public int getIdUser() {
		return idUser;
	}

	public String getPrenomUser() {
		return prenomUser;
	}

	public String getNomUser() {
		return nomUser;
	}

	public String getAdressUser() {
		return adressUser;
	}

	public int getNumTelUser() {
		return numTelUser;
	}

	public String getMailUser() {
		return mailUser;
	}

	public ArrayList<Rent> getUserRents() {
		return userRents;
	}

	// setter

	public void setPrenomUser(String prenomUser) {
		this.prenomUser = prenomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public void setAdressUser(String adressUser) {
		this.adressUser = adressUser;
	}

	public void setNumTelUser(int numTelUser) {
		this.numTelUser = numTelUser;
	}

	public void setMailUser(String mailUser) {
		this.mailUser = mailUser;
	}

	public void setUserRents(ArrayList<Rent> userRents) {
		this.userRents = userRents;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

}


import java.util.ArrayList;

public class Book {
	private int idBook;
	private String titleBook;
	private String authorBook;
	private String editorBook;
	private int pageNbBook;
	private String summaryBook;
	private Library library;

	public Book(String titleBook, String authorBook, String editorBook, int pageNbBook, String summaryBook,
			Library library) {
		super();

		
		this.titleBook = titleBook;
		this.authorBook = authorBook;
		this.editorBook = editorBook;
		this.pageNbBook = pageNbBook;
		this.summaryBook = summaryBook;
		this.library = library;

	}

	public Book() {

	}

// afficher book
	@Override
	public String toString() {
		return "le livre n� : " + idBook + " son titre est '"+titleBook+"', son auteur est '" +authorBook+ "',son editeur est "+editorBook+",son nombre de pase est "
				+  pageNbBook + ", son resum� est ' " + summaryBook + "', appartient � la biblioth�que " + library.getNameLibrary() + '\n';
	}

// comparer deux instances book
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (authorBook == null) {
			if (other.authorBook != null)
				return false;
		} else if (!authorBook.equals(other.authorBook))
			return false;
		if (editorBook == null) {
			if (other.editorBook != null)
				return false;
		} else if (!editorBook.equals(other.editorBook))
			return false;
		if (library == null) {
			if (other.library != null)
				return false;
		} else if (!library.equals(other.library))
			return false;
		if (pageNbBook != other.pageNbBook)
			return false;
		if (summaryBook == null) {
			if (other.summaryBook != null)
				return false;
		} else if (!summaryBook.equals(other.summaryBook))
			return false;
		if (titleBook == null) {
			if (other.titleBook != null)
				return false;
		} else if (!titleBook.equals(other.titleBook))
			return false;
		return true;
	}

//getter

	public String getAuthorBook() {
		return authorBook;
	}

	public void setAuthorBook(String authorBook) {
		this.authorBook = authorBook;
	}

	public int getIdBook() {
		return idBook;
	}

	public String getTitleBook() {
		return titleBook;
	}

	public String getEditorBook() {
		return editorBook;
	}

	public int getPageNbBook() {
		return pageNbBook;
	}

	public String getSummaryBook() {
		return summaryBook;
	}

	public Library getLibrary() {
		return library;
	}

	// setter

	public void setIdBook(int idBook) {
		this.idBook = idBook;
	}

	public void setTitleBook(String titleBook) {
		this.titleBook = titleBook;
	}

	public void setEditorBook(String editorBook) {
		this.editorBook = editorBook;
	}

	public void setPageNbBook(int pageNbBook) {
		this.pageNbBook = pageNbBook;
	}

	public void setSummaryBook(String summaryBook) {
		this.summaryBook = summaryBook;
	}

	public void setLibrary(Library library) {
		this.library = library;
	}

}

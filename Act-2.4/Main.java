

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class Main {

	public static void main(String[] args) throws SQLException {
		 List<Book> books = LibraryManager.displayBooks("Biblioth�que du village");
		 for(Book b : books) {
			 System.out.println(b);
			
		 }
		 
		 Library lib1 = new Library(1,"Biblioth�que du village", "05 chemin du village", "0531256897"); 
		 Book book1 = new Book("aaaaaa", "cccc", "mmm", 10, "iedeioeejkefek",lib1);
		System.out.println(LibraryManager.addBook ( book1));
		
		System.out.println("**********sortByAuthor***********************"+ '\n');
		List<Book> sortedBooks = LibraryManager.SortBooksbyAuthor();
		 System.out.println("la liste des livres tri�s par auteur est : ");
		 for(Book b : sortedBooks) {
			 System.out.println(b);
		 }
		 
		 //Une m�thode permettant de rechercher un livre par son ID;
		 System.out.println("*********************SearchBookById***********"+ '\n');
		 System.out.println(LibraryManager.SearchBookById(1));
	
		 System.out.println("*****************************searchBookByAUthor*****************************************************"+ '\n');
		  List<Book> book2 = LibraryManager.searchBookByAUthor("Kathy Sierra");
		  for (Book b :book2) {
			  System.out.println(b);
		  }
		  if (book2.isEmpty()) System.out.println("pas de livre pour cet auteur");
		  
		  System.out.println("*****************************searchUserById*****************************************************");
		System.out.println("Les details du user sont: " +"\n"+LibraryManager.searchUserById (1));
		
		 System.out.println("*****************************Create User*****************************************************");
		   User user1 = new User( "ttt", "hhh", "jjjj", 142536,"addsd@qsdq.coms");
		   System.out.println(LibraryManager.addUser(user1));
		 System.out.println("insertion sucess");
		 
		 System.out.println("***************create Rent***********************");
		 java.util.Date date = new Date();
		 java.sql.Timestamp currentdate = new java.sql.Timestamp(date.getTime());
		 long tenDay = 10 * 24 * 60 * 60 * 1000;
		 java.sql.Timestamp  dateFin= new java.sql.Timestamp(date.getTime()+tenDay);
		 Rent newRent = new Rent(currentdate,dateFin,user1,book1);
		 System.out.println( LibraryManager.createRent(newRent));
		
		 System.out.println("insert rent avec succes");
		 
		 System.out.println("***************sortRent By Descending order***********************");
		 String biblio = "Biblioth�que du village";
		 List<Rent>rents =  LibraryManager.sortRentByDate( biblio);
		 System.out.println("la liste des prets ordonn� de la bibliotheque '" + biblio + "' sont :");
		 for (Rent r : rents) {
			 System.out.println("le pret n� "+ r.getIdRent()+" doit etre remis le " +r.getDateFin() );
		 }
//		
	}

}

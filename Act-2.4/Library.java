
import java.util.ArrayList;

public class Library {

	private int idLibrary;
	private String nameLibrary;
	private String adressLibrary;
	private String numTelLibrary;
	private ArrayList<Rent> rentLirary;
	private ArrayList<Book> booksLibrary;

	public Library() {

	}

	public Library(int idLibrary, String nameLibrary, String adressLibrary, String numTelLibrary) {
		super();
		this.idLibrary = idLibrary;
		this.nameLibrary = nameLibrary;
		this.adressLibrary = adressLibrary;
		this.numTelLibrary = numTelLibrary;
//		this.rentLirary = rentLirary;
//		this.booksLibrary = booksLibrary;
	}

	// getter

	public int getIdLibrary() {
		return idLibrary;
	}

	public String getNameLibrary() {
		return nameLibrary;
	}

	public String getAdressLibrary() {
		return adressLibrary;
	}

	public String getNumTelLibrary() {
		return numTelLibrary;
	}

	public ArrayList<Rent> getRentLirary() {
		return rentLirary;
	}

	public ArrayList<Book> getBooksLibrary() {
		return booksLibrary;
	}

	// setter

	public void setIdLibrary(int idLibrary) {
		this.idLibrary = idLibrary;
	}

	public void setNameLibrary(String nameLibrary) {
		this.nameLibrary = nameLibrary;
	}

	public void setAdressLibrary(String adressLibrary) {
		this.adressLibrary = adressLibrary;
	}

	public void setNumTelLibrary(String numTelLibrary) {
		this.numTelLibrary = numTelLibrary;
	}

	public void setRentLirary(ArrayList<Rent> rentLirary) {
		this.rentLirary = rentLirary;
	}

	public void setBooksLibrary(ArrayList<Book> booksLibrary) {
		this.booksLibrary = booksLibrary;
	}

}

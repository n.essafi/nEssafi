package com.activite4;

public class Bateau {

	private int idBateau;
	private String nameBateau;
	private String marqueBateau;
	private static int nbreBateau = 1;
	private Cabine cabine;
	private Coque coque;
	private Pont pont;

	public Bateau(String nameBateau, String marqueBateau) {

		this.idBateau = nbreBateau++;
		this.nameBateau = nameBateau;
		this.marqueBateau = marqueBateau;
		this.cabine = new Cabine(25.0, 10.0);
		this.coque = new Coque("plat", "bois");
		this.pont = new Pont("red", "plastique");
	}

	// getter and setter

	public int getIdBateau() {
		return idBateau;
	}

	public void setIdBateau(int idBateau) {
		this.idBateau = idBateau;
	}

	public String getNameBateau() {
		return nameBateau;
	}

	public void setNameBateau(String nameBateau) {
		this.nameBateau = nameBateau;
	}

	public String getMarqueBateau() {
		return marqueBateau;
	}

	public void setMarqueBateau(String marqueBateau) {
		this.marqueBateau = marqueBateau;
	}

	public static int getNbreBateau() {
		return nbreBateau;
	}

	public static void setNbreBateau(int nbreBateau) {
		Bateau.nbreBateau = nbreBateau;
	}

	public void affiche() {
		System.out.println("je suis le bateau");
	}

	public Cabine getCabine() {
		return cabine;
	}

	public void setCabine(Cabine cabine) {
		this.cabine = cabine;
	}

	public Coque getCoque() {
		return coque;
	}

	public void setCoque(Coque coque) {
		this.coque = coque;
	}

	public Pont getPont() {
		return pont;
	}

	public void setPont(Pont pont) {
		this.pont = pont;
	}

	@Override
	public String toString() {

		return "Le bateau n� " + this.getIdBateau() + " est constitu� de :" + "\n" + "La coque n�"
				+ this.coque.getIdCoque() + " ;" + "\n" + "La cabine n�" + this.cabine.getIdCabine() + " ;" + "\n"
				+ "Le pont n�" + this.pont.getIdPont() + " est consitut� du Mat n�" + Mat.getNbreMat()
				+ " qui est constitu� de la Voile N�" + Voile.getNbreVoile() + " ;";

	}

}
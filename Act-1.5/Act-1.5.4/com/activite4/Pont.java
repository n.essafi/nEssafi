package com.activite4;

public class Pont {

	private static int nbrePont = 4;
	private String couleur;
	private String matiere;
	private int idPont;
	private Mat mat;

	public Pont(String couleur, String matiere) {

		this.couleur = couleur;
		this.matiere = matiere;
		this.idPont = nbrePont++;
		this.setMat(new Mat("gray", 10.0));
	}
	
	
//getter and setter
	public static int getNbrePont() {
		return nbrePont;
	}

	public static void setNbrePont(int nbrePont) {
		Pont.nbrePont = nbrePont;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public int getIdPont() {
		return idPont;
	}

	public void setIdPont(int idPont) {
		this.idPont = idPont;
	}

	public void affiche() {
		System.out.println("je suis le pont");
	}

	public Mat getMat() {
		return mat;
	}

	public void setMat(Mat mat) {
		this.mat = mat;
	}

	@Override
	public String toString() {
		return "Le pont n� " + this.getIdPont();
	}

}
package  com.activite4;
public class Mat {

	private static int nbreMat = 4;
	private String couleurMat;
	private double longueurMat;
	private int idMat;
	private Voile voile;

	public Mat(String couleurMat, double longueurMat) {
		
		this.couleurMat = couleurMat;
		this.longueurMat = longueurMat;
		this.idMat = nbreMat++;
		this.voile = new Voile(500.0, "v5522", "blue");
	}
	
//getter and setter
	public static int getNbreMat() {
		return nbreMat;
	}

	public static void setNbreMat(int nbreMat) {
		Mat.nbreMat = nbreMat;
	}

	public String getCouleurMat() {
		return couleurMat;
	}

	public void setCouleurMat(String couleurMat) {
		this.couleurMat = couleurMat;
	}

	public double getLongueurMat() {
		return longueurMat;
	}

	public void setLongueurMat(double longueurMat) {
		this.longueurMat = longueurMat;
	}

	public int getIdMat() {
		return idMat;
	}

	public void setIdMat(int idMat) {
		this.idMat = idMat;
	}

	public Voile getVoile() {
		return voile;
	}

	public void setVoile(Voile voile) {
		this.voile = voile;
	}

	public void affiche() {
		System.out.println("je suis le mat");
	}

	@Override
	public String toString() {
		return "Le Mat n° " + this.idMat;
	}

}

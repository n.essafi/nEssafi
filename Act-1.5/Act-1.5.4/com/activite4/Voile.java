package com.activite4;
public class Voile {
	
	
	private static int nbreVoile = 5;
	private int idVoile;
	private double prixVoile;
	private String referenceVoile;
	public String couleurVoile;
	
	
	public Voile( double prixVoile, String referenceVoile, String couleurVoile) {
		
		this.idVoile = nbreVoile++;
		this.prixVoile = prixVoile;
		this.referenceVoile = referenceVoile;
		this.couleurVoile = couleurVoile;
	
	}

    //getter and setter
	public static int getNbreVoile() {
		return nbreVoile;
	}
	public static void setNbreVoile(int nbreVoile) {
		Voile.nbreVoile = nbreVoile;
	}
	public int getIdVoile() {
		return idVoile;
	}
	public void setIdVoile(int idVoile) {
		this.idVoile = idVoile;
	}
	public double getPrixVoile() {
		return prixVoile;
	}
	public void setPrixVoile(double prixVoile) {
		this.prixVoile = prixVoile;
	}
	public String getReferenceVoile() {
		return referenceVoile;
	}
	public void setReferenceVoile(String referenceVoile) {
		this.referenceVoile = referenceVoile;
	}
	public String getCouleurVoile() {
		return couleurVoile;
	}
	public void setCouleurVoile(String couleurVoile) {
		this.couleurVoile = couleurVoile;
	}
	@Override
	public String toString() {
		return "La voile n� "+ this.idVoile;
	}


	
}

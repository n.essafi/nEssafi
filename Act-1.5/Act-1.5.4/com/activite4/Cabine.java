package  com.activite4;

public class Cabine {

	private static int nbreCabine = 3;
	private double longueurCabine;
	private double largeurCabine;
	private int idCabine;

	public Cabine(double longueurCabine, double largeurCabine) {

		this.longueurCabine = longueurCabine;
		this.largeurCabine = largeurCabine;
		this.idCabine = nbreCabine++;
	}
	
	//getter and setter

	public static int getNbreCabine() {
		return nbreCabine;
	}

	public static void setNbreCabine(int nbreCabine) {
		Cabine.nbreCabine = nbreCabine;
	}

	public double getLongueurCabine() {
		return longueurCabine;
	}

	public void setLongueurCabine(double longueurCabine) {
		this.longueurCabine = longueurCabine;
	}

	public double getLargeurCabine() {
		return largeurCabine;
	}

	public void setLargeurCabine(double largeurCabine) {
		this.largeurCabine = largeurCabine;
	}

	public int getIdCabine() {
		return idCabine;
	}

	public void setIdCabine(int idCabine) {
		this.idCabine = idCabine;
	}

	public void affiche() {
		System.out.println("je suis la cabine");
	}

	@Override
	public String toString() {
		return "La cabine n� " + this.getIdCabine();
	}

}
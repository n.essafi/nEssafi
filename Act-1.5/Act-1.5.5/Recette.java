public class Recette {
	
		private String nomRecette;
		private String descriptin;
		private int difficulte;

		private String[] etapes = new String[20];
		private String[] astuces = new String[20];
		


		public Recette() {
			super();
		}

		public Recette(String nomRecette, String descriptin, int difficulte ) {
			this.setNomRecette( nomRecette);
			this.setDescriptin( descriptin);
			this.setDifficulte( difficulte);
		}

		

		public String getNomRecette() {
			return nomRecette;
		}

		private void setNomRecette(String nomRecette) {
			this.nomRecette = nomRecette;
		}

		public String getDescriptin() {
			return descriptin;
		}

		private void setDescriptin(String descriptin) {
			this.descriptin = descriptin;
		}

		public int getDifficulte() {
			return difficulte;
		}

		private void setDifficulte(int difficulte) {
			this.difficulte = difficulte;
		}

		public String[] getEtapes() {
			return etapes;
		}

	

		public String[] getAstuces() {
			return astuces;
		}

		

		public void addAstuce(String astuce) {
			int i =0;
			while(i<astuces.length) {
				astuces[i] = astuce;
			}
			
			i++;
		}
		public void addEtape(String etape) {
			int i =0;
			while(i<etapes.length) {
				etapes[i] = etape;	
			}
			
			i++;
		}


		public void Afficher() {
			System.out.println("Recette : " + this.nomRecette + "\n" +
		    "de difficulté " + this.difficulte+ "\n" +
					"nDescription: "+this.descriptin);
			System.out.println("les Etapes : ");
			for(String val:this.etapes){
				System.out.println(val);
			}
			
			System.out.println("les astuces : ");
			for(String val : astuces){
				System.out.println(val);
			}
			
		
		}

	}
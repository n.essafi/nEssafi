public class Livre {
	

	private String titre;
	private String auteur;
	private double prix;
	private int nombreDePages;
	
	public Livre() {
		super();
	}
	
	public Livre(String titre, String auteur, double prix, int nombreDePages) {
		super();
		this.setTitre(titre);
		this.setAuteur(auteur);
		this.setPrix( prix);
		this.setNombreDePages(nombreDePages);
	}

	public String getTitre() {
		return titre;
	}

	private void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	private void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public double getPrix() {
		return prix;
	}

	private void setPrix(double prix) {
		this.prix = prix;
	}

	public int getNombreDePages() {
		return nombreDePages;
	}

	private void setNombreDePages(int nombreDePages) {
		this.nombreDePages = nombreDePages;
	}
	
	
	@Override
	public String toString() {
		String str= "Livre [titre=" + titre + ", auteur=" + auteur + ", prix=" + prix + ", nombreDePages=" + nombreDePages
				+ "]";
		 return str;
	}

	public void Afficher() {
		System.out.println("le titre du livre est " + this.titre + " son auteurest " + this.auteur
				+ " son nombre de pages est " +  this.nombreDePages + " . et son prix est:" + this.prix+ " dollars");
		
	}
	
	

}


public class BD extends Livre {
	
	private boolean color;
	
	public BD() {
		super();
	}
	

	public BD(String titre, String auteur, double prix, int nombreDePages, boolean color) {
		super(titre,   auteur,  prix,  nombreDePages);
		this.setColor(color);
	}

	public boolean isColor() {
		return color;
	}

	private void setColor(boolean color) {
		this.color = color;
	}
	

	public void Afficher() {
		
		super.Afficher();
		if(this.color)
		{
			System.out.println("cette bande dessiné est en couleur ");
		}else {
			
		System.out.println("cette bande dessiné est en noir ");
		}

		
	}


	@Override
	public String toString() {
		return "BD [color=" + color + "]";
	}
		
	
}

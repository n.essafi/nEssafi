public class Roman extends Livre {
	
	private int nbChapitre;
    private String  description;
    
    
    
	public Roman() {
		super();
		
	}
	
	public Roman(String titre, String auteur, double prix, int nombreDePages) {
		super( titre, auteur, prix, nombreDePages);
	
	}

	public int getNbChapitre() {
		return nbChapitre;
	}
	public void setNbChapitre(int nbChapitre) {
		this.nbChapitre = nbChapitre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void Afficher()
	{	
		super.Afficher();
		System.out.println("Le roman est composé de " + this.nbChapitre + "chapitres." +"\n"+
		"son resume :\n " + 
		this.description);
	}

    

}
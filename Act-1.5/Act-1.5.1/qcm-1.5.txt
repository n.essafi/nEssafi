

QCM 1 - Pourquoi utilisons-nous les classes ?

f)Pour augmenter les performances du programme.

QCM 2 - Qu'est-ce qu'une classe ?
f)Une variable avec des méthodes.

QCM 3 - Qu'est-ce qu'un attribut d'une Class ?

a)Une variable qui définit un de ses états, une propriété;


QCM 4 - Comment identifier une Class ?
b)On identifie dans la problématique un objet répondant aux critères;

QCM 5 - Comment définir les attribut d'une Class ?

a)On identifie ce qui décrit et est propre un objet;

QCM 6 - Comment définir les fonctions d'une Class ?

e)On identifie les actions communes à tous les objets;

QCM 7 - Comment utiliser une Class ?

MaClasse maClasse = new MaClass();
a)On la déclare, on l'instancie et on l'utilise;

QCM 8 - Quelle affirmation est juste ?

d)Le code ne marche pas, une exception est levée;


QCM 9 - Que représente l'encapsulation ?

c)La portée des éléments de la class;


QCM 10 - Qu'est-ce qu'un constructeur ?

d)L'élément qui permet d'instancier une classe;



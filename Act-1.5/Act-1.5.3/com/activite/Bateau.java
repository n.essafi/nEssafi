package com.activite;

public class Bateau {

	private int idBateau;
	private String nameBateau;
	private String marqueBateau;
	private static int nbreBateau = 1;

	public Bateau() {

		this.marqueBateau = "aquamar";
		this.nameBateau = "Titanic";
	}

	public Bateau(String nameBateau, String marqueBateau) {
		super();
		this.idBateau = nbreBateau++;
		this.nameBateau = nameBateau;
		this.marqueBateau = marqueBateau;
	}

	// getter and setter
	public int getIdBateau() {
		return idBateau;
	}

	public void setIdBateau(int idBateau) {
		this.idBateau = idBateau;
	}

	public String getNameBateau() {
		return nameBateau;
	}

	public void setNameBateau(String nameBateau) {
		this.nameBateau = nameBateau;
	}

	public String getMarqueBateau() {
		return marqueBateau;
	}

	public void setMarqueBateau(String marqueBateau) {
		this.marqueBateau = marqueBateau;
	}

	public static int getNbreBateau() {
		return nbreBateau;
	}

	public static void setNbreBateau(int nbreBateau) {
		Bateau.nbreBateau = nbreBateau;
	}

	public void affiche() {
		System.out.println("je suis le bateau");
	}

}
package com.activite;
public class Coque {

	private int idCoque;
	private String fond;
	private String matiere;
	private static int nbreCoque = 3;

	public Coque() {
		this.fond = "plat";
		this.matiere = "bois";
	}

	public Coque(String fond, String matiere) {

		this.idCoque = nbreCoque++;
		this.fond = fond;
		this.matiere = matiere;
	}

//getter and setter
	public int getIdCoque() {
		return idCoque;
	}

	public void setIdCoque(int idCoque) {
		this.idCoque = idCoque;
	}

	public String getFond() {
		return fond;
	}

	public void setFond(String fond) {
		this.fond = fond;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public static int getNbreCoque() {
		return nbreCoque;
	}

	public static void setNbreCoque(int nbreCoque) {
		Coque.nbreCoque = nbreCoque;
	}

	public void affiche() {
		System.out.println("je suis le coque");
	}

}
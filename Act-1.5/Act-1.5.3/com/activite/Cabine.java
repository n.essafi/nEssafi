package com.activite;
public class Cabine {

	private int idCabine;
	private double longueur;
	private double largeur;
	private static int nbreCabine = 2;

	public Cabine() {
		this.idCabine = nbreCabine++;
		this.largeur = 10.0;
		this.longueur = 20.0;
	}

	public Cabine(double longueur, double largeur) {

		this.idCabine = nbreCabine++;
		this.longueur = longueur;
		this.largeur = largeur;
	}

// getter and setter
	public int getIdCabine() {
		return idCabine;
	}

	public void setIdCabine(int idCabine) {
		this.idCabine = idCabine;
	}

	public double getLongueur() {
		return longueur;
	}

	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	public double getLargeur() {
		return largeur;
	}

	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}

	public static int getNbreCabine() {
		return nbreCabine;
	}

	public static void setNbreCabine(int nbreCabine) {
		Cabine.nbreCabine = nbreCabine;
	}

	public void affiche() {
		System.out.println("je suis la cabine");
	}

}

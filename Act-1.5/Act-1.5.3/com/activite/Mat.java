package com.activite;
public class Mat {

	private int idMat;
	private String couleur;
	private double longueur;
	private static int nbreMat = 4;

	public Mat() {
		this.couleur = "red";
		this.longueur = 5.0;
	}

	public Mat(String couleur, double longueur) {

		this.idMat = nbreMat++;
		this.couleur = couleur;
		this.longueur = longueur;
	}
	

	//getter and setter
	public int getIdMat() {
		return idMat;
	}

	public void setIdMat(int idMat) {
		this.idMat = idMat;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public double getLongueur() {
		return longueur;
	}

	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}

	public static int getNbreMat() {
		return nbreMat;
	}

	public static void setNbreMat(int nbreMat) {
		Mat.nbreMat = nbreMat;
	}

	public void affiche() {
		System.out.println("je suis le mat");
	}

}

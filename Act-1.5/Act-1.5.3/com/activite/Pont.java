package com.activite;

public class Pont {

	private int idPont;
	private String couleur;
	private String matiere;
	private static int nbrePont = 5;

	public Pont() {
		this.couleur = "blanc";
		this.matiere = "plastique";

	}

	public Pont(String couleur, String matiere) {

		this.idPont = nbrePont++;
		this.couleur = couleur;
		this.matiere = matiere;
	}

	// getter and setter
	public int getIdPont() {
		return idPont;
	}

	public void setIdPont(int idPont) {
		this.idPont = idPont;
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	public String getMatiere() {
		return matiere;
	}

	public void setMatiere(String matiere) {
		this.matiere = matiere;
	}

	public static int getNbrePont() {
		return nbrePont;
	}

	public static void setNbrePont(int nbrePont) {
		Pont.nbrePont = nbrePont;
	}

	public void affiche() {
		System.out.println("je suis le pont");
	}

}
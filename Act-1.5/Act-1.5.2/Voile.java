public class Voile {

	private double prixVoile;
	private String referenceVoile;
	public String couleurVoile;
	protected int longueurVoile;
	private boolean enroule, deroule;

	public Voile() {

		this.prixVoile = 100.0;
		this.referenceVoile = "vo5462";
		this.couleurVoile = "red";
		this.longueurVoile = 10;

	}

	public Voile(double prixVoile, String referenceVoile, String couleurVoile, int longueurVoile, boolean enroule,
			boolean deroule) {
		super();
		this.prixVoile = prixVoile;
		this.referenceVoile = referenceVoile;
		this.couleurVoile = couleurVoile;
		this.longueurVoile = longueurVoile;
		this.enroule = enroule;
		this.deroule = deroule;
	}

	// getter and setter

	public double getPrixVoile() {
		return prixVoile;
	}

	public void setPrixVoile(double prixVoile) {
		this.prixVoile = prixVoile;
	}

	public String getReferenceVoile() {
		return referenceVoile;
	}

	public void setReferenceVoile(String referenceVoile) {
		this.referenceVoile = referenceVoile;
	}

	public String getCouleurVoile() {
		return couleurVoile;
	}

	public void setCouleurVoile(String couleurVoile) {
		this.couleurVoile = couleurVoile;
	}

	public int getLongueurVoile() {
		return longueurVoile;
	}

	public void setLongueurVoile(int longueurVoile) {
		this.longueurVoile = longueurVoile;
	}

	public void derouler() {
		this.setDeroule(true);
		this.setEnroule(false);

	}

	public boolean isEnroule() {
		return enroule;
	}

	private void setEnroule(boolean enroule) {
		this.enroule = enroule;
	}

	public boolean isDeroule() {
		return deroule;
	}

	private void setDeroule(boolean deroule) {
		this.deroule = deroule;
	}

	public void enrouler() {
		this.setEnroule(true);
		this.setDeroule(false);

	}

}

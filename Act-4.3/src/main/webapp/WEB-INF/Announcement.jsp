<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/../css/custom.css"%></style>
</head>
<body>
	<header>
		<jsp:include page="header.html"></jsp:include>
	</header>

	<h3 class="text-center text-white pt-5">Login form</h3>

	<div class="container">
		<div id="login-row"
			class="row justify-content-center align-items-center">
			<div id="login-column" class="col-md-10 login">
			 <h3 class="text-center text-info">Announcement list</h3>
			 <br>
				<table class="table">
					<caption>Announcement list</caption>
					<thead>
						<tr>
							<th scope="col">Title</th>
							<th scope="col">Price</th>
							<th scope="col">Description</th>
							<th scope="col">Localisation</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="announce" items="${announcements}">
							<tr>

								<td>${announce.titleAd}</td>
								<td>${announce.priceAd}</td>
								<td>${announce.descriptionAd}</td>
								<td>${announce.localisationAd}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>


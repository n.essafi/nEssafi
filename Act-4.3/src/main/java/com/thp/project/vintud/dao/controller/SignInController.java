package com.thp.project.vintud.dao.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.thp.project.vintud.entity.impl.SignInForm;

@WebServlet("/SignInController")
public class SignInController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    

	public static final String VUE_SUCCES = "/WEB-INF/Home.jsp";
	public static final String VUE_FORM = "/WEB-INF/SignIn.jsp";
	
	public SignInController() {
		super();

	}


	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher("/WEB-INF/SignIn.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* Pr�paration de l'objet formulaire */
		SignInForm form = new SignInForm();
		/* Ajout du bean et de l'objet m�tier � l'objet requ�te */
		request.setAttribute("form", form);
	
		if (form.getErreurs().isEmpty()) {
			/* Si aucune erreur, alors affichage de la fiche r�capitulative */
			
			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);

		} else {
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}

	}

}

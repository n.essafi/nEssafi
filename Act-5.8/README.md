**Pré-requis :**
Un environnement d'exécution Java 8 (Java Runtime Environment) doit être installé et configuré dans la variable système PATH.

les API Rest sont testés avec Mock

Un OS Windows ou Linux


**Description** :
cette application restful ayant comme retour sous format Json, permet de gerer un site de vente en ligne:
- creer un utilisateur
- creer une annonce
- recuperer les annonces disponible par l'utilisateur
- attribuer un role à un utilisateur pour limiter ses actions
- filtrer et trier les recherche



package com.thp.project.vintud;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = SpringBootProjectApplication.class)
@AutoConfigureMockMvc
@Transactional
public class DemoApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	UserRepository userRepo;

	
	// test API create user 
	@Test
	public void testCreateUser() throws Exception {

		User user = new User();
		user.setIdUser(1);
		user.setMail("test@gmail.com");
		user.setPassword("root");

		mvc.perform(post("/users/signin").content(asJsonString(user)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.mail", is("test@gmail.com")));

	}

	
	// test ApI postAd
	@Test
	public void testPostAd() throws Exception {
		Ad ad = new Ad();
		ad.setIdAd(1);
		ad.setAvailabilityAd(true);
		ad.setTitleAd("bebe");
		mvc.perform(post("/users/1/ads").content(asJsonString(ad)).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.idUser", is(1)));
	}

	// test API getAllAds filtred by title
	@Test
	public void testGetAllads() throws Exception {
		mvc.perform(get("/ads?title=bebe")).andExpect(status().isOk());
	}

	
	// test ApI getAllMyAd
	@Test
	public void testGetMyads() throws Exception {

		mvc.perform(get("/users/1")).andExpect(status().isOk());

	}

	public static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(obj);
			return jsonContent;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}

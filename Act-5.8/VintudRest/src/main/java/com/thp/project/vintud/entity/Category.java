package com.thp.project.vintud.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int idCategory;
	
	@Column(name = "name")
	private String nameCategory;
	
	@Column(name = "description")
	private String descriptionCategory;
	
	public Category() {
	}

	public Category(String nameCategory, String descriptionCategory) {
		this.nameCategory = nameCategory;
		this.descriptionCategory = descriptionCategory;
		
	}

	public String getNameCategory() {
		return nameCategory;
	}

	public String getDescriptionCategory() {
		return descriptionCategory;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setNameCategory(String nameCategory) {
		this.nameCategory = nameCategory;
	}

	public void setDescriptionCategory(String descriptionCategory) {
		this.descriptionCategory = descriptionCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

}

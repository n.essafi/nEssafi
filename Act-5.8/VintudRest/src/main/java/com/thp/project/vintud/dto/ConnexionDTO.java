package com.thp.project.vintud.dto;

public class ConnexionDTO {

	private int idUser;
	private String password;
	private String mail;
	
	
	public int getIdUser() {
		return idUser;
	}


	public String getPassword() {
		return password;
	}


	public String getMail() {
		return mail;
	}


	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public ConnexionDTO() {
		
	}
}

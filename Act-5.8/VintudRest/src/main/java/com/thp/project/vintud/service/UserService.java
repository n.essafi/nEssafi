package com.thp.project.vintud.service;

import java.util.List;

import com.thp.project.vintud.dto.ConnexionDTO;
import com.thp.project.vintud.dto.AdDTO;
import com.thp.project.vintud.dto.UserDTO;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.Favorite;

public interface UserService {
	
	List<UserDTO> findUsers();
	UserDTO postAd(int iduser, AdDTO adDTO);
	UserDTO updateAd(int idUser, AdDTO adDTO);
	UserDTO createUser(UserDTO userDto);
	boolean login(ConnexionDTO connexionDTO);
	UserDTO update(int idUser, UserDTO userDto);
	UserDTO addFavorite(int iduser, int idAd);
	List<Favorite> getFavoriteAds(int iduser);
	List<AdDTO> getAllMyAds(int iduser);


}

package com.thp.project.vintud.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.thp.project.vintud.entity.Ad;

@Transactional(propagation = Propagation.MANDATORY)
@Repository
public interface AdRepository extends JpaRepository<Ad, Long> , JpaSpecificationExecutor<Ad>{


	List<Ad> findByAvailabilityAdAndOwnerIdUserIsNot(boolean b, int userId);

	List<Ad> findByTitleAdAndCategoryAdNameCategory(String title, String category);

	List<Ad> findByTitleAd(String title);

	List<Ad> findByCategoryAdNameCategory(String category);

	Optional<Ad> findByIdAd(int idAd);



}

package com.thp.project.vintud.service.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.thp.project.vintud.helper.ModelMapperConverter;
import com.thp.project.vintud.dto.ConnexionDTO;
import com.thp.project.vintud.dto.AdDTO;
import com.thp.project.vintud.dto.UserDTO;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.Favorite;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.AdRepository;
import com.thp.project.vintud.repository.FavoriteRepository;
import com.thp.project.vintud.repository.UserRepository;
import com.thp.project.vintud.service.UserService;

@Transactional(propagation = Propagation.REQUIRES_NEW)
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserRepository userRepository;
	@Autowired
	AdRepository adRepository;
	@Autowired
	FavoriteRepository favoriteRepository;
	

	@Override
	public List<UserDTO> findUsers() {
		return ModelMapperConverter.convertUserAllToDTO(userRepository.findAll());
		
	}
	@Override
	public UserDTO postAd(int iduser, AdDTO adDTO) {
		User u = userRepository.findById(iduser).orElse(null);
		Ad ad = ModelMapperConverter.convertToAd(adDTO);
		if (u != null) {
			ad.setOwner(u);
			u.getPostedAds().add(ad);
			userRepository.save(u);
			return ModelMapperConverter.converUserToDTO(u);
		}
		return null;

	}

	@Override

	public UserDTO updateAd(int idUser, AdDTO adDTO) {
		User u = userRepository.findById(idUser).orElse(null);
		Ad ad = ModelMapperConverter.convertToAd(adDTO);
		if ((u != null) & (ad != null) & (adDTO.getIdowner() == idUser)) {
			u.getPostedAds().add(ad);
			userRepository.save(u);
			
			return ModelMapperConverter.converUserToDTO(u);
		}
		return null;
	}

	public UserDTO createUser(UserDTO userDto) {
		User user = ModelMapperConverter.convertToUser(userDto);
		List<User> existingUser = userRepository.findByMail(user.getMail());		
		if (existingUser.isEmpty()) {
		return ModelMapperConverter.converUserToDTO(userRepository.save(user));
		}
		return null;
	}
	
	@Override
	public boolean login(ConnexionDTO connexionDTO) {
		boolean istrue;
		List<User> result = userRepository.findByMailAndPassword(connexionDTO.getMail(),connexionDTO.getPassword());
		if (result.isEmpty()) {
			istrue = false;
		} else {
			istrue = true;
		}
		return istrue;	
	}
	
	@Override
	public UserDTO update(int idUser, UserDTO userDto) {
		User userNewInformation = ModelMapperConverter.convertToUser(userDto);
		User userToUpdate = userRepository.getOne(idUser);
		
		if(userNewInformation.getAddress()!=null) {
			userToUpdate.setAddress(userNewInformation.getAddress());
		}
		if(userNewInformation.getPassword()!=null) {
		userToUpdate.setPassword(userNewInformation.getPassword());
		}
		
		if(userNewInformation.getPseudo()!=null) {
		userToUpdate.setPseudo(userNewInformation.getPseudo());
		}
		
		if(userNewInformation.getMail()!=null) {
		userToUpdate.setMail(userNewInformation.getMail());
		}
		 return ModelMapperConverter.converUserToDTO(userRepository.save(userToUpdate));
		 
	}
	
		
	@Override
	public UserDTO addFavorite(int iduser, int idAd) {
		User u = userRepository.findById(iduser).orElse(null);
		Ad ad = adRepository.findByIdAd(idAd).orElse(null);
		if (u != null && ad != null) {
			Favorite favorite = new Favorite();
	        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	        favorite.setFavouriteDate(timestamp);
			favorite.setFavouriteAd(ad);
			favorite.setFavouriteUser(u);
			u.getFavoriteAds().add(favorite);
			userRepository.save(u);
			return ModelMapperConverter.converUserToDTO(u);
		}
		return null;

	}
	@Override
	public List<Favorite> getFavoriteAds(int iduser) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public List<AdDTO> getAllMyAds(int iduser) {
		User u = userRepository.findById(iduser).orElse(null);
	if (u != null) {
		return ModelMapperConverter.convertAllAdToDTO(u.getPostedAds());	
	}
		return null;
	}
	
//	@Override
//	public List<Favorite> getFavoriteAds(int iduser) {
//		User u = userRepository.findById(iduser).orElse(null);
//		if (u != null) {
//		List<Favorite> favorites= u.getFavoriteAds();
//		/*List<Ad> ads = null;
//		for (Favorite favAd : favorites) 
//		{ 
//		    ads.add(adRepository.findByIdAd(favAd.getIdFavourite()).get());
//		}
//		
//		//return ModelMapperConverter.convertAllAdToDTO(ads);
//		}*/
//		
//		return favorites;
//	}
	


		
		
		
	

	

}

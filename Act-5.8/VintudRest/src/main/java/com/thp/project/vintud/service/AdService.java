package com.thp.project.vintud.service;

import java.util.List;
import com.thp.project.vintud.dto.AdDTO;


public interface AdService {

	List<AdDTO> findAllAds();
	List<AdDTO> findAvailableAdsByuser(int userId);
	List<AdDTO> findByNameAndCateg(String name, String category);
	List<AdDTO> findByName(String title);
	List<AdDTO> findByCateg(String category);

}

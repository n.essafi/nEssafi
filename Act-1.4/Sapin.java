import java.util.Scanner;		
	  
	class Sapin { 
		
		
		public static void main(String[] args) { 


        Scanner sc = new Scanner(System.in);
        System.out.println(" Combien de triangles ? ");
        int numberTriangle =sc.nextInt();

            
		    printMultipleSapin(numberTriangle);
		   
		    printTronc(5,4);
		} 
	  
	
	static void printSpace(int space) { //fonction pour dessiner les espaces
	    
	    if (space == 0) {
	    	 return; 
	    }else {
	    	System.out.print(" "); // pour mettre les espaces
	    }
	       
	    printSpace(space - 1);  
	} 
	  
	
	static void printStar(int star) {  //fonction pour dessiner les etoil
	    
	    if (star == 0) {
	    	return;
	    }else {
	    	 System.out.print("* ");
	    }
	         
	   
	    printStar(star - 1); 
	} 
	  
	
	
	static void printSapin(int n, int num) { // fonction pour dessiner les spapins
	   
	    if (n == 0) {
	    	 return;
	    }
	        
	    printSpace(2*n - 1); 
	    printStar((num - n)*2 + 1); 
	    System.out.println(""); 
	  
	    
	    printSapin(n - 1, num); 
	} 
	  
	  static void printTronc(int n ,int k) { // fonction pour dessiner le tronc du sapin
	   
	    if (k> 0) {   
	    printSpace(n); 
	    printStar(n-2 ); 
	    System.out.println(""); 
	   
	    
	    printTronc(n,k -1); 
	     
	   }
	} 
	static void printMultipleSapin(int k) { // fonction pour plusieurs sapin
	   
	    if (k> 0) {  
	    printSapin(4, 4); 
	   
	    
	    printMultipleSapin(k -1); 
	     
	   }
	}
	 
    
} 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<style><%@include file="/../css/custom.css"%></style>
<body>


	<jsp:include page="header.html"></jsp:include>
	<div class="container">
		<div id="login-row"
			class="row justify-content-center align-items-center">
			<div id="login-column" class="col-md-6 home">
				<h1 style="text-align: center">Welcome to your global second-hand community A free marketplace for
				pre-loved fashion, powered by 25 million members across Europe.</h1>
				<div class="card">
					<div class="card-body">
						Welcome to our website E-commerce "<b>"${message}"</b>"

					</div>
				</div>
				<div class="website-description">
					<p>With our website you can :</p>
					<ul>
						<li>Create an announcement</li>
						<li>Consult the list of announcements</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</body>

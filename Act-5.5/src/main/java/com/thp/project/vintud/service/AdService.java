package com.thp.project.vintud.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;

@Repository
public interface AdService {

	void createAd(Ad ad);

	List<Ad> getAvailableAds(boolean i);

	List<Ad> getAdsByCategory(int categoryid);

	List<Ad> filterAdsByPrice(double min, double max);

	List<Ad> filterAdsLocation(String location);

	void updateAd(Ad announce);

	void delete(Ad ad);

	void deleteById(int idTodelete);

	List<Ad> display();

	List<Ad> getAdByUserId(int id);

	List<Ad> getAdByUser(User user);

}

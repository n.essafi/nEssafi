package com.thp.project.vintud.service;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Category;

@Repository
public interface CategoryService {

	void createCategory(Category category);

	void delete(Category category);

	List<Category> display();

	Category getCategory(int id);

}

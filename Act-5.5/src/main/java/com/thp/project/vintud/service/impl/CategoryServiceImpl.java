package com.thp.project.vintud.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Category;
import com.thp.project.vintud.repository.CategoryRepository;
import com.thp.project.vintud.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;

	public void createCategory(Category category) {

		categoryRepository.save(category);

	}

	// supprimer categorie
	@Override
	public void delete(Category category) {
		categoryRepository.delete(category);

	}

	// afficher la liste des categorie
	@Override
	public List<Category> display() {

		return categoryRepository.findAll();
	}

	// afficher categorie par id
	@Override
	public Category getCategory(int id) {

		return categoryRepository.findByIdCategory(id);
	}

}

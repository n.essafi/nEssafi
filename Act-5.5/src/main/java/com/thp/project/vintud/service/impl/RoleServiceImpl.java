package com.thp.project.vintud.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.Roles;
import com.thp.project.vintud.repository.RoleRepository;
import com.thp.project.vintud.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	// ajouter un role
	public void createRole(Role role) {

		roleRepository.save(role);

	}

	// supprimer un role
	@Override
	public void delete(Role role) {
		roleRepository.delete(role);

	}

	// trouver role par id
	@Override
	public Role getbyId(int id) {

		return roleRepository.findByIdRole(id);
	}

	// afficherbla liste des roles
	@Override
	public List<Role> display() {

		return roleRepository.findAll();
	}

}

package com.thp.project.vintud.service;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.thp.project.vintud.entity.Role;

@Repository
public interface RoleService {

	void createRole(Role role);
	
	 void delete(Role role);
	 
	 public Role getbyId(int id);
	 	 
	 List<Role> display();

}

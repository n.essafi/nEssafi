package com.thp.project.vintud.main;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.thp.project.vintud.config.AppConfig;
import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.Category;
import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.Roles;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.service.AdService;
import com.thp.project.vintud.service.CategoryService;
import com.thp.project.vintud.service.RoleService;
import com.thp.project.vintud.service.SearchService;
import com.thp.project.vintud.service.UserService;

public class MainApp {

	public static void main(String[] args) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

		UserService userService = context.getBean(UserService.class);
		RoleService roleService = context.getBean(RoleService.class);
		AdService adService = context.getBean(AdService.class);
		CategoryService categoryService = context.getBean(CategoryService.class);
		SearchService searchService = context.getBean(SearchService.class);

		Role roleUSer = new Role(Roles.CLIENT);
		roleService.createRole(roleUSer);

		User user = new User("firstName", "lastName", " pseudo", "admin", "admin@gmail.com", 12, " address", roleUSer);
		userService
				.createUser(new User("firstName", "lastName", " pseudo", "password", "mail", 12, " address", roleUSer));

		System.out.println("Connexion ");
		userService.login("admin@gmail.com", "admin");

		userService.updateUser(user, "nejah@talan.com", "pseudo2", "pasword2");

		System.out.println("create Category");
		Category category = new Category("chemise", "bébé");
		categoryService.createCategory(category);

		Date date = new Date();
		Timestamp datePublication = new Timestamp(date.getTime());
		Ad ad = new Ad("chemise garçon", "picture", "chemise bleu nouveau né", 100, datePublication, true, "Tunis", 10,
				user, category);
		adService.createAd(ad);

		System.out.println("les annonces valides");

		Iterable<Ad> all = adService.getAvailableAds(true);
		//all.forEach(System.out::println);

		System.out.println("les annonces by category ");

		Iterable<Ad> searchAnnonceCategory = adService.getAdsByCategory(3);
		//searchAnnonceCategory.forEach(System.out::println);

		System.out.println("les annonces by price ");

		Iterable<Ad> filtredAdbyprice = adService.filterAdsByPrice(50, 1110);
		//filtredAdbyprice.forEach(System.out::println);

		System.out.println("les annonces bylocalisation ");

		Iterable<Ad> filtredAdbyLocalisation = adService.filterAdsLocation("Tunis");
		//filtredAdbyLocalisation.forEach(System.out::println);

		System.out.println("les annonces byUser id ");

		Iterable<Ad> filtredAdbyUserid = adService.getAdByUserId(1);
		//filtredAdbyUserid.forEach(System.out::println);
		
		System.out.println("les annonces byUser  ");

		Iterable<Ad> filtredAdbyUser = adService.getAdByUser(user);
		//filtredAdbyUser.forEach(System.out::println);

	}

}

package com.thp.project.vintud.service;

import java.util.List;

import org.springframework.stereotype.Repository;
import com.thp.project.vintud.entity.Role;
import com.thp.project.vintud.entity.User;

@Repository
public interface UserService {

	void createUser(User user);

	boolean login(String mail, String password);

	void updateUser(User user, String mail, String pseudo, String password);

	void updateRole(User user, Role role);

	void delete(User user);

	User getbyId(int id);

	List<User> display();

}

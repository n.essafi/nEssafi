package com.thp.project.vintud.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;

@Repository
@Transactional
public interface AdRepository extends JpaRepository<Ad, Long> {

	List<Ad> findByAvailabilityAd(boolean i);

	List<Ad> findByCategoryAdIdCategory(int categoryid);

	List<Ad> findByPriceAdBetween(double min, double max);

	List<Ad> findByLocalisationAd(String location);

	void deleteByIdAd(int idTodelete);

	List<Ad> findByUserAdIdUser(int id);

	List<Ad> findByUserAd(User user);

}

package com.thp.project.vintud.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.thp.project.vintud.entity.Ad;
import com.thp.project.vintud.entity.User;
import com.thp.project.vintud.repository.AdRepository;
import com.thp.project.vintud.service.AdService;

@Service
public class AdServiceImpl implements AdService {

	@Autowired
	private AdRepository adRepository;

	// ajouter annonce
	public void createAd(Ad ad) {
		adRepository.save(ad);
	}

	// trouver les annonces encore disponible
	@Override
	public List<Ad> getAvailableAds(boolean i) {
		return adRepository.findByAvailabilityAd(i);

	}

	// trouver les annonces par categorie
	@Override
	public List<Ad> getAdsByCategory(int categoryid) {
		return adRepository.findByCategoryAdIdCategory(categoryid);

	}

	// filtrer les annonce par prix
	@Override
	public List<Ad> filterAdsByPrice(double min, double max) {

		return adRepository.findByPriceAdBetween(min, max);

	}

	// filtrer les annonces par location
	@Override
	public List<Ad> filterAdsLocation(String location) {
		return adRepository.findByLocalisationAd(location);

	}

	// supprimer annonce
	@Override
	public void delete(Ad ad) {
		adRepository.delete(ad);
	}

	// supprimer annonce par identifiant
	@Override
	public void deleteById(int idTodelete) {
		adRepository.deleteByIdAd(idTodelete);
	}

	// mise ajour annonce
	@Override
	public void updateAd(Ad announce) {
		adRepository.save(announce);

	}

	// afficher tous les annonces
	@Override
	public List<Ad> display() {
		return adRepository.findAll();
	}
	
	// afficher tous les annonces by user id
		@Override
		public List<Ad> getAdByUserId(int id) {
			return adRepository.findByUserAdIdUser(id);

		}
		
		// afficher tous les annonces by user object
		@Override
		public List<Ad> getAdByUser(User user) {
			return adRepository.findByUserAd(user);

		}
		
	

}

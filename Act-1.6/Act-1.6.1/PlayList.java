
public interface PlayList {

	public String toString();

	public void addMusicToPlaylist(MusiqueImpl music);

	public void deleteDoubleMusic();

	public void displaySameMusic(PlayListeImpl playlist);

	public void triMusicByAuthor();
	
	public void triMusicByInterpreter();

}

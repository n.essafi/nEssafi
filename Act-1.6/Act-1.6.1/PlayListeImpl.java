import java.util.Arrays;

public class PlayListeImpl implements PlayList {

	final int MAX_MUSIQUES;
	private MusiqueImpl[] musicPlaylist;
	private String genre;
	private String nom;
	private int nbrTitrePlayList;

	public PlayListeImpl(int maxMusiques, String genre, String nom) {

		MAX_MUSIQUES = maxMusiques;
		this.musicPlaylist = new MusiqueImpl[10];
		this.genre = genre;
		this.nom = nom;
		this.nbrTitrePlayList = 0;
	}

	public String toString() {
		return this.nom + ", de genre " + this.genre + " contient " + this.nbrTitrePlayList + " Titre(s) : " + "\n"
				+ Arrays.toString(this.musicPlaylist) + "]";
	}

	@Override
	public void addMusicToPlaylist(MusiqueImpl music) {

		this.musicPlaylist[this.nbrTitrePlayList] = music;

		this.nbrTitrePlayList++;

	}

	@Override
	public void deleteDoubleMusic() {

		for (int i = 0; i < this.nbrTitrePlayList; i++) {

			for (int j = i + 1; j < this.nbrTitrePlayList; j++) {

				if (this.musicPlaylist[i].equals(this.musicPlaylist[j])) {

					// on replace l'element duppliqu� avec le dernier element unique
					this.musicPlaylist[j] = this.musicPlaylist[this.nbrTitrePlayList - 1];

					this.musicPlaylist[this.nbrTitrePlayList - 1] = null; // on affecte a cet element le valeur null

					this.nbrTitrePlayList--;

					j--;
				}

			}

		}

	}

	@Override
	public void displaySameMusic(PlayListeImpl playlist) {

		for (int i = 0; i < this.nbrTitrePlayList; i++) {

			for (int j = 0; j < playlist.nbrTitrePlayList; j++) {

				if (this.musicPlaylist[i].equals(playlist.musicPlaylist[j])) {

					System.out.println("la musique commune est : " + this.musicPlaylist[i]);

				}
			}
		}
	}

	@Override
	public void triMusicByAuthor() {

		for (int i = this.nbrTitrePlayList - 1; i > 0; i--) {

			for (int j = 0; j < i; j++) {

				if (this.musicPlaylist[j].getMusicAuthor()
						.compareToIgnoreCase((this.musicPlaylist[j + 1]).getMusicAuthor()) > 0)

					swap(this.musicPlaylist, j, j + 1);
			}
		}

	}

	public void triMusicByInterpreter() {

		for (int i = this.nbrTitrePlayList - 1; i > 0; i--) {

			for (int j = 0; j < i; j++) {

				if (this.musicPlaylist[j].getMusicInterpreter()
						.compareToIgnoreCase((this.musicPlaylist[j + 1]).getMusicInterpreter()) > 0)

					swap(this.musicPlaylist, j, j + 1);
			}
		}

	}

	public static void swap(MusiqueImpl[] array, int i, int j) {

		MusiqueImpl temp = array[i];
		array[i] = array[j];
		array[j] = temp;

	}

}


public class MusiqueImpl implements Musique {

	private String musicTitle;
	private String musicAuthor;
	private String musicInterpreter;
	private String musicGender;

	public MusiqueImpl(String title, String author, String interpreter, String gender) {

		this.musicTitle = title;
		this.musicAuthor = author;
		this.musicInterpreter = interpreter;
		this.musicGender = gender;
	}

	@Override
	public boolean equals(Musique obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MusiqueImpl other = (MusiqueImpl) obj;
		if (musicAuthor == null) {
			if (other.musicAuthor != null)
				return false;
		} else if (!musicAuthor.equals(other.musicAuthor))
			return false;
		if (musicGender == null) {
			if (other.musicGender != null)
				return false;
		} else if (!musicGender.equals(other.musicGender))
			return false;
		if (musicInterpreter == null) {
			if (other.musicInterpreter != null)
				return false;
		} else if (!musicInterpreter.equals(other.musicInterpreter))
			return false;
		if (musicTitle == null) {
			if (other.musicTitle != null)
				return false;
		} else if (!musicTitle.equals(other.musicTitle))
			return false;
		return true;
	}

	public String toString() {
		return " [Titre=" + this.musicTitle + ", auteur=" + this.musicAuthor + ", Interprete=" + this.musicInterpreter
				+ ", Gender=" + this.musicGender + "]";
	}

	public String getMusicAuthor() {
		return musicAuthor;
	}

	public void setMusicAuthor(String musicAuthor) {
		this.musicAuthor = musicAuthor;
	}

	public String getMusicInterpreter() {
		return musicInterpreter;
	}

	public void setMusicInterpreter(String musicInterpreter) {
		this.musicInterpreter = musicInterpreter;
	}

}

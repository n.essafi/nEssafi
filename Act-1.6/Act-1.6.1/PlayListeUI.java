
public class PlayListeUI {

	public static void main(String[] args) {

		MusiqueImpl m1 = new MusiqueImpl("titre1", " Nejah", " mahdi", " jazz");
		MusiqueImpl m2 = new MusiqueImpl("titre2", " mohamed", " Adel", " jazz");
		MusiqueImpl m3 = new MusiqueImpl("titre3", " yAssine", " Adam", " jazz");
		MusiqueImpl m4 = new MusiqueImpl("titre4", " rania", " amir", " jazz");

		// System.out.println(m1);
		System.out.println(m1.equals(m2));

		System.out.println("****************************************************************************************");

		PlayListeImpl playlist1 = new PlayListeImpl(10, "house", "album 1");

		playlist1.addMusicToPlaylist(m1);
		playlist1.addMusicToPlaylist(m2);
		playlist1.addMusicToPlaylist(m3);
		playlist1.addMusicToPlaylist(m4);
		System.out.println("playlist 1 : " + playlist1);

		System.out.println("*///////////////////////////////////////////////////////////////////////");

		playlist1.triMusicByAuthor(); // trie du playliste par auteur
		System.out.println("*************playlist1 tri� par auteur " + "\n" + playlist1);

		System.out.println("*///////////////////////////////////////////////////////////////////////");

		playlist1.triMusicByInterpreter(); // trie du playliste par interpreteur
		System.out.println("*************playlist1 tri� par ordre croissant par Interpreteur " + "\n" + playlist1);

		System.out.println(
				"**********************************************************************************************");
		PlayListeImpl playlist2 = new PlayListeImpl(10, "house", "album2");
		playlist2.addMusicToPlaylist(m1);
		playlist2.addMusicToPlaylist(m2);

		System.out.println("playlist 2 : " + playlist2);
		System.out.println(
				"***************************************************************************************************");
		// supprimer les doublons
		playlist1.deleteDoubleMusic();
		System.out.println(playlist1);

		System.out.println("**********************************************************");
		// afficher les musiques en commun
		playlist1.displaySameMusic(playlist2);

	}

}

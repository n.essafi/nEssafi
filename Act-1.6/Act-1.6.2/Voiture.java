
public class Voiture extends Vehicule {

	public Voiture(int anneeModeleVehicule, double prixVehicule, int puissanceVehicule) {
		super(anneeModeleVehicule, prixVehicule, puissanceVehicule);

	}

	@Override
	public void accelerer() {

		System.out.println("La voiture de " + this.getAnneeModeleVehicule() + ", matricule " + this.getNumeroMatricule()
		+ " acc�l�re!");

	}

	@Override
	public void demarrer() {
		System.out.println( "La voiture de " + this.getAnneeModeleVehicule() + ", matricule " + this.getNumeroMatricule()
		+ " d�marre!");

	}

}

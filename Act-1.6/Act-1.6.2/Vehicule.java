
public abstract class Vehicule {

	private int numeroMatricule;
	private int anneeModeleVehicule;
	private double prixVehicule;
	private int puissanceVehicule;

	static int nbrVehiculeCree = 0;

	public Vehicule(int anneeModeleVehicule, double prixVehicule, int puissanceVehicule) {

		this.numeroMatricule = ++nbrVehiculeCree;
		this.anneeModeleVehicule = anneeModeleVehicule;
		this.prixVehicule = prixVehicule;
		this.puissanceVehicule = puissanceVehicule;

	}

	public abstract void accelerer();

	public abstract void demarrer();

	@Override
	public String toString() {


			return "Un(e) "+ this.getClass().getName()+" de " + this.anneeModeleVehicule + ", matricule " + this.numeroMatricule
					+ ", d'une puissance de " + this.puissanceVehicule + " CV et au prix de " + this.prixVehicule
					+ " euros a �t� enregistr�e.";

	}

	public int getNumeroMatricule() {
		return numeroMatricule;
	}

	public void setNumeroMatricule(int numeroMatricule) {
		this.numeroMatricule = numeroMatricule;
	}

	public int getAnneeModeleVehicule() {
		return anneeModeleVehicule;
	}

	public void setAnneeModeleVehicule(int anneeModeleVehicule) {
		this.anneeModeleVehicule = anneeModeleVehicule;
	}

	public double getPrixVehicule() {
		return prixVehicule;
	}

	public void setPrixVehicule(double prixVehicule) {
		this.prixVehicule = prixVehicule;
	}

	public double getPuissanceVehicule() {
		return puissanceVehicule;
	}

	public void setPuissanceVehicule(int puissanceVehicule) {
		this.puissanceVehicule = puissanceVehicule;
	}

}

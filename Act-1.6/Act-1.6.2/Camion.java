
public class Camion extends Vehicule {

	public Camion(int anneeModeleVehicule, double prixVehicule, int puissanceVehicule) {
		super(anneeModeleVehicule, prixVehicule, puissanceVehicule);

	}

	@Override
	public void accelerer() {

		System.out.println( "Le Camion de " + this.getAnneeModeleVehicule() + ", matricule " + this.getNumeroMatricule()
		+ " acc�l�re!");

	}

	@Override
	public void demarrer() {
		System.out.println("Le Camion de " + this.getAnneeModeleVehicule() + ", matricule " + this.getNumeroMatricule()
		+ " d�marre!");

	}

}
